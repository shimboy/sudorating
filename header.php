<?php

if (!isset($_SESSION)){ session_start(); }
require_once "php/google_config.php";
$login = $client->createAuthUrl();

?>

<!doctype html>
<html lang="en-US">

<head>
	<?php if (strpos($_SERVER['PHP_SELF'], 'website.php') !== false){ ?>
	<title><?php echo $website_name; ?></title>
	<meta name="description" content="<?php echo $meta_desc; ?>">
	<?php }else{ ?>
    <title>Sudoseo</title>
	<?php } ?>
    <meta charset='utf-8' />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="https://www.sudoseo.com/css/autocomplete.css" rel="stylesheet">
	<link rel="stylesheet" media="screen" href="https://www.sudoseo.com/css/theme.css" />
	<link rel="stylesheet" media="screen" href="https://www.sudoseo.com/css/styles.css" />
         
	<style>
	.progresss{position:relative;height:4px;display:block;width:100%;background-color:#acece6;border-radius:2px;margin:.5rem 0 1rem 0;overflow:hidden}.progresss .determinate{position:absolute;top:0;left:0;bottom:0;background-color:#26a69a;-webkit-transition:width .3s linear;transition:width .3s linear}.progresss .indeterminate{background-color:#26a69a}.progresss .indeterminate:before{content:'';position:absolute;background-color:inherit;top:0;left:0;bottom:0;will-change:left, right;-webkit-animation:indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite;animation:indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite}.progresss .indeterminate:after{content:'';position:absolute;background-color:inherit;top:0;left:0;bottom:0;will-change:left, right;-webkit-animation:indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;animation:indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;-webkit-animation-delay:1.15s;animation-delay:1.15s}@-webkit-keyframes indeterminate{0%{left:-35%;right:100%}60%{left:100%;right:-90%}100%{left:100%;right:-90%}}@keyframes indeterminate{0%{left:-35%;right:100%}60%{left:100%;right:-90%}100%{left:100%;right:-90%}}@-webkit-keyframes indeterminate-short{0%{left:-200%;right:100%}60%{left:107%;right:-8%}100%{left:107%;right:-8%}}@keyframes indeterminate-short{0%{left:-200%;right:100%}60%{left:107%;right:-8%}100%{left:107%;right:-8%}}
	#search::-webkit-input-placeholder {
		color: #888;
	}
	#search:focus::-webkit-input-placeholder {
		color: #333;
	}
	#search:focus, #search:focus{
		outline: none;
	}
	</style>
<?php if (strpos($_SERVER['PHP_SELF'], 'website.php') !== false){ ?>
<style>
.easy-autocomplete{
  width:70% !important
}

.easy-autocomplete input{
  width: 70%;
}
</style>
<?php } ?>
</head>
<body>

<nav class="navbar navbar-expand-lg">
  <div class="container">
  <a class="navbar-brand" href="https://www.sudoseo.com">SUDOSEO</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse " id="navbarSupportedContent">
  <?php
	if (strpos($_SERVER['PHP_SELF'], 'website.php') !== false){
	?>
	<form id="form" style="width:50%" class="form-inline my-lg-0">
      <input id="search" style="width:100%;padding:20px;color:#333;background:#2a384f;border:2px solid #888" class="search form-control mr-sm-2" type="search" placeholder="Search for a website.." aria-label="Search" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search for a website..'">
	  <div class="autocomplete-suggestions" style="width:70%;position: absolute; display: none; z-index: 9999;"></div>
	</form>
	<?php
	}
	?>
    <ul class="navbar-nav ml-auto">
      <!-- <li class="nav-item active">
        <a class="nav-link" href="#">Categories <span class="sr-only">(current)</span></a>
      </li> -->
	  <?php if (!isset($_SESSION['login'])){ ?>
      <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#login">Log in</a>
      </li>
	  <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#signin">Sign up</a>
      </li>
	  <?php } ?>
    </ul>
	<?php if (isset($_SESSION['login'])){ ?>
	<ul class="navbar-nav">
	  <li style="margin-left:30px" class="nav-item dropdown">
       <a class="profile-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img style="margin-top:-5px;border-radius:50%;width:30px;height:30px;margin-right:5px" src="<?php echo $_SESSION['gravatar']; ?>"> <?php if (strpos($_SESSION['name'], ' ') !== false) {echo substr($_SESSION['name'], 0, strrpos($_SESSION['name'], ' ')); }else{ echo $_SESSION['name']; } ?></a>
      <div style="margin-top:5px;background:#2a384f;color:#fff;border:none;border-radius:0" class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="https://www.sudoseo.com/account">Dashboard</a>
          <a class="dropdown-item" href="https://www.sudoseo.com/my_websites">My Websites</a>
          <a class="dropdown-item" href="https://www.sudoseo.com/settings">Settings</a>
          <a class="dropdown-item" href="https://www.sudoseo.com/logout">Log out</a>
        </div>
	  </li>
    </ul>
	<?php } ?>
  </div>
  </div>
</nav>