<?php

require "php/user_functions.php";
require_once "php/google_config.php";

if (isset($_SESSION['access_token'])){
	$client->setAccessToken($_SESSION['access_token']);
}else if (isset($_GET['code'])){
	$token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
	$_SESSION['access_token'] = $token;
}

$auth = new Google_Service_Oauth2($client);
$user = $auth->userinfo_v2_me->get();

create_user_google($user->name, $user->email, md5("changeme"), "Please select", date("Y-m-d"), $_SERVER['REMOTE_ADDR']);
header("Location: account");

