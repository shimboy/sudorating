<?php

if (!isset($_SESSION)){ session_start(); }
if (!isset($_SESSION['login'])){ header("Location: index.php"); }

require_once("php/functions.php");
require_once("php/user_functions.php");
include "header.php";

$data = getProfileData($_SESSION['id']);

?>

<div class="wrapper">
<div class="rp_top_bar">
	<div class="rp_top_bar_container container">
<div class="container emp-profile">
            <form method="post">
                <div class="row" style="display:flex;align-items:center">
                    <div class="col-md-2">
                        <div class="profile-img">
                            <img style="border-radius:50%" src="<?php echo $data['gravatar']; ?>">
							<div style="height:15px">&nbsp;</div>
						</div>
                    </div>
                    <div class="col-md-6">
                        <div class="profile-head">
                                    <h5>
                                        <?php echo ucwords($data['name']); ?>
                                    </h5>
                                    <h6>
                                        <?php echo $data['country']; ?>
                                    </h6>
									<div style="height:15px">&nbsp;</div>
                        </div>
                    </div>
					<div class="col-md-4">
					<div style="text-align:center">
						<div style="float:left;margin-right:20%">
						<p class="proile-rating"><?php getReviewCount($data['id']); ?> <div style="margin-top:10px">Reviews</div></p>
						</div>
						<div style="float:left;margin-right:20%">
						<p class="proile-rating"><?php getReadCount($data['id']); ?> <div style="margin-top:10px">Reads</div></p>
						</div>
						<div style="float:left">
						<p class="proile-rating"><?php getLikeCount($data['id']); ?> <div style="margin-top:10px">Likes</div></p>
						</div>
						</div>
						<div class="clearfix"></div>
					</div>
                </div>
			</form>
</div>
</div>
</div>

<?php if ($data['verified'] == 0){ ?>
<br>
<div class="container">
<div class="alert alert-danger" role="alert">
<div style="margin-top:10px"><strong>Verify email address</strong></div>
<p>Please verify your email by clicking the link within the email we sent you.</p>
</div>
</div>
<?php }else{ ?>
<br>
<?php } ?>

<div class="homepage_background_image">
	<div style="background:none" class="main container-fluid">
		<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div id="review_list">
				<?php 
				if (!empty(getUserReviews($data['id']))){
				foreach(getUserReviews($data['id']) as $review){
					$website = getWebsiteData($review['website_id']);
				?>
				<div id="<?php echo $review['id']; ?>" style="width:100%" class="lr website_top_box container mt-sm-3">
					<div class="row">
						<div class="col-12">
							<div style="margin-bottom:15px;padding:10px;background:#f2f2f2">
							Review of <a href="website/<?php echo $website['name']; ?>" style="color:#79a9d9"><?php echo ucwords(str_replace(array("https://", "http://", "www."), "", $website['name'])); ?></a>
							</div>
							<div style="display:flex;align-items:center">
							<div style="float:left">
								<img src="<?php echo $data['gravatar']; ?>" class="rounded-circle" alt="Cinque Terre" style="border:1px solid #e2e2e2;width:45px;height:45px;margin-right:10px">
							</div>
							<div style="float:left">
								<div><?php echo ucwords($data['name']); ?></div>
							</div>
							</div>
							<div class="clearfix"></div>
							<hr>
							<div class="row">
								<div class="col-6">
									<div style="margin-bottom:5px;margin-top:5px" class="form-group">
										<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
										  <i class="fa fa-star"></i>
										</button>
										<button type="button" class="btn btn-sm <?php if ($review['rating'] > 1){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
										  <i class="fa fa-star"></i>
										</button>
										<button type="button" class="btn btn-sm <?php if ($review['rating'] > 2){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
										  <i class="fa fa-star"></i>
										</button>
										<button type="button" class="btn btn-sm <?php if ($review['rating'] > 3){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
										  <i class="fa fa-star"></i>
										</button>
										<button type="button" class="btn btn-sm <?php if ($review['rating'] > 4){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
										  <i class="fa fa-star"></i>
										</button>
									</div>
								</div>
								<div class="col-6 text-right">
									<span style="color:#777"><?php echo get_time_ago(strtotime($review['time'])); ?></span>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-12">
									<div style="font-size:16px;font-weight:500"><?php echo $review['title']; ?></div>
									<div class="mt-sm-2" style="font-size:14px"><?php echo $review['review']; ?></div>
								</div>
							</div>
							
							<hr>
							
							<div class="row">
								<div class="col-12">
									<div style="float:left;margin-right:20px">
									<a data-id="<?php echo $review['id']; ?>" class="edit e_review" href=""><i class="fa fa-pencil"></i> Edit</a>
									</div>
									<div style="float:left;margin-right:20px">
									<a data-id="<?php echo $review['id']; ?>" class="delete edit" href=""><i class="fa fa-trash"></i> Delete</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php 
				}
				}else{
				?>
				<div style="width:100%;text-align:center" class="website_top_box container mt-sm-3">
					<div class="row">
						<div class="col-12">
							<h3>No reviews yet</h3>
						</div>
					</div>
				</div>
				<?php
				}
				?>
				</div>
			</div>
			<div class="col-md-4">
			<div class="website_top_box container mt-sm-3">
				<h3>Top Rated Sites</h3>
				<hr>
				<div style="width:100%;background:#f0f0f0;padding:10px;margin-bottom:10px">
					<?php 
					$count = 0;
					foreach(top_rated_sites() as $key => $val){
						$count++;
						if ($count > 10) break;
						$website = getWebsiteData($key);
					?>
					<div style="width:99%;margin:-left aut;margin-right:auto;margin-bottom:10px;background:#fff;padding:10px">
					<div class="row">
						<div class="col-12">
								<div class="row">
									<div class="col-md-10">
										<div style="float:left">
											<a style="color:#333" href="website/<?php echo $website['name']; ?>"><img src="<?php echo $website['screenshot']; ?>" style="margin-right:2px;border:1px solid #e2e2e2;max-height:30px"></a>
										</div>
										<div style="float:left;margin-left:10px">
											<h3 style="font-size:1em"><a style="color:#333" href="website/<?php echo $website['name']; ?>"><?php 
											$name = ucwords($website['name']);
											echo substr($name, 0, strpos($name, "."));
											?></a></h3>
										</div>
									</div>
									<div class="col-md-2">
										<div class="rating-value"><?php echo $val; ?></div>
									</div>
								</div>
						</div>
					</div>
					</div>
					<?php } ?>
					</div>
			</div>
			
			<div class="website_top_box container mt-sm-3">
				<div class="row">
					<div class="col-md-12">
						<div style="width:120px;height:600px;border:1px solid #e2e2e2;margin:0 auto">
							<img src="img/skyscraper.png">
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
<br><br>
</div>
</div>

<div id="m_edit" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      
    </div>
  </div>
</div>

<?php include "footer.php"; ?>

<script>
$(document).on("click", ".delete", function(e){
	e.preventDefault();
	var id = $(this).attr("data-id");
	$.post("php/delete_review.php", {id:id}, function(data){
		if (data == "success"){
			$('#'+id).slideUp();
			setTimeout(function(){
				$('#'+id).remove();
				if ($('.lr').length == 0){
					$('#review_list').html(
						'<div style="width:100%;text-align:center" class="website_top_box container mt-sm-3">'+
							'<div class="row">'+
								'<div class="col-12">'+
									'<h3>No reviews yet</h3>'+
								'</div>'+
							'</div>'+
						'</div>'
					);
				}
			}, 500);
		}else{
			alert("Sorry, you can not delete this review");
		}
	});
});
$(document).on("click", ".e_review", function(e){
	e.preventDefault();
	var id = $(this).attr("data-id");
	$.get("php/review_data.php", {id:id}, function(data){
		$('.modal-content').html(data);
		$('#m_edit').modal("show");
	});
});
</script>
</body>
</html>