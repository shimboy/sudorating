<?php

include "header.php";
require_once("php/functions.php");
require_once("php/user_functions.php");

?>
<div class="homepage_background_image">
	<div style="padding:0" class="main container-fluid">
	<div id="blue" style="background:#6ea6e6">
	<div class="htext"><br><br><br><br></div>
	<div class="text-center">
		<h1 class="homepage_h1" style="margin-bottom:0;color:#fff">Behind every review is an experience that matters</h1>
		<h3 class="homepage_h3 mt-sm-1" style="color:#fff">Read reviews. Write reviews. Find companies. Submit websites.</h3> 
		<h3 class="homepage_h3_mobile" style="color:#fff"><br><br>Search for a website below</h3>
	</div>
	<br><br>
	<div class="mt-sm-5">
		<form id="form" style="border:1px solid #e2e2e2" class="hero__search-form search-form" role="search" action="" method="get" autocomplete="false" data-hero-search-form="">
            <span class="hero__search-form__placeholder-icon icon-search"></span>
            <input id="search" class="hero__search-input search-input" type="search" name="query" data-home-search-input="" placeholder="Search for a website" aria-label="Search for a companyâ€¦" autofocus="" autocomplete="off">
            <button class="hero__search-form__submit" type="submit" aria-label="Search">
                <span class="hero__search-form__submit__text">Search</span>
                <span class="hero__search-form__submit__icon icon-search"></span>
            </button>
			<div class="autocomplete-suggestions" style="position: absolute; display: none; z-index: 9999;"></div>
		</form>
	</div>
	<div class="htext"><br><br><br><br></div><br><br>
	</div>
	<br>
	<div class="container mt-sm-4">
		
		</div>
		</div>
	</div>
	<br><br>
	</div>
</div>

<?php include "footer.php"; ?>

</body>
</html>