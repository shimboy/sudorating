<?php

session_start();
require_once("functions.php");

$id = $_GET['id'];

$sql = $con->prepare("SELECT * FROM reviews WHERE id = :id");
$sql->bindParam(":id", $id);
$sql->execute();
$review = $sql->fetch(PDO::FETCH_ASSOC);

$website = getWebsiteData($review['website_id']);

?>
<div class="modal-header">
<h5 class="modal-title">Edit review for <?php echo $website['name']; ?></h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<div>
	<h3 style="color:#777">Rating</h3>
	<div style="margin-bottom:5px;margin-top:5px" class="form-group">
		<button type="button" class="btn btn-warning btn-sm rateButton" aria-label="Left Align">
		  <i class="fa fa-star"></i>
		</button>
		<button type="button" class="btn btn-sm rateButton <?php if ($review['rating'] > 1){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
		  <i class="fa fa-star"></i>
		</button>
		<button type="button" class="btn btn-sm rateButton <?php if ($review['rating'] > 2){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
		  <i class="fa fa-star"></i>
		</button>
		<button type="button" class="btn btn-sm rateButton <?php if ($review['rating'] > 3){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
		  <i class="fa fa-star"></i>
		</button>
		<button type="button" class="btn btn-sm rateButton <?php if ($review['rating'] > 4){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
		  <i class="fa fa-star"></i>
		</button>
	</div>
	
	<input type="hidden" id="rating" value="<?php echo $review['rating']; ?>">
	
	<br>
	
	<h3 style="color:#777">Review title</h3>
	<input style="color:#777" id="title" type="text" class="form-control" value="<?php echo $review['title']; ?>">
	
	<br>
	
	<h3 style="color:#777">Your review</h3>
	<textarea id="review" style="width:100%;padding:10px;color:#777" rows="10"><?php echo $review['review']; ?></textarea>
</div>
</div>
<div class="modal-footer">
<button id="submit" type="button" class="btn btn-primary">Edit review</button>
</div>
<script>
$(function(){
	$( ".rateButton" ).click(function(){
		if($(this).hasClass('btn-grey')) {			
			$(this).removeClass('btn-grey btn-default').addClass('btn-warning star-selected');
			$(this).prevAll('.rateButton').removeClass('btn-grey btn-default').addClass('btn-warning star-selected');
			$(this).nextAll('.rateButton').removeClass('btn-warning star-selected').addClass('btn-grey btn-default');			
		} else {						
			$(this).nextAll('.rateButton').removeClass('btn-warning star-selected').addClass('btn-grey btn-default');
		}
		$("#rating").val($('.star-selected').length);		
	});
	
	$('#submit').click(function(e){
	  e.preventDefault();
	  var title = $('#title').val();
	  var review = $('#review').val();
	  var website_id = '<?php echo $review['website_id']; ?>';
	  var rating = $('#rating').val();
	  var name = '<?php echo $_SESSION['name']; ?>';
	  var id = '<?php echo $review['id']; ?>';
	  var website_id = '<?php echo $review['website_id']; ?>';
	  var website_name = '<?php echo ucwords(str_replace(array("https://", "http://", "www."), "", $website['name'])); ?>';
	  
	  if (rating == 0){ rating = 1; }
	  
	  var btn2 = "";
	  var btn3 = "";
	  var btn4 = "";
	  var btn5 = "";
	  
	  if(rating > 1){ btn2 = "btn-warning"; }else{ btn2 = "btn-default btn-grey"; }
	  if(rating > 2){ btn3 = "btn-warning"; }else{ btn3 = "btn-default btn-grey"; }
	  if(rating > 3){ btn4 = "btn-warning"; }else{ btn4 = "btn-default btn-grey"; }
	  if(rating > 4){ btn5 = "btn-warning"; }else{ btn5 = "btn-default btn-grey"; }
	  
	  if (title == "" || review == ""){
		  alert("Please fill in all information and try again");
	  }else{
		  $.post("php/edit_review.php", {title:title, review:review, website_id:website_id, rating:rating, id:id}, function(data){
			location.reload();
		  });
	  }
  });
});
</script>