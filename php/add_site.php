<?php

require_once("functions.php");

if (!isset($_POST['search'])){ exit; }
$url = $_POST['search'];

if (is_domain_valid($url)){
	$url = preg_replace('#((?:https?://)?[^/]*)(?:/.*)?$#', '$1', $url);
	$realUrl = getEfectiveUrl($url);
	$meta = getMetaData($url, $real_url);
	
	if ($meta != "error"){
		$speed_desktop = getSpeedData($realUrl, "desktop", true, 40, $realUrl);
		$speed_mobile = getSpeedData($realUrl, "mobile", true, 40, $realUrl);
		$moz = domainAuthority($url);
		$traffic = getTrafficAndEarnings($url);
		$bounce = 0;
		
		if ($speed_desktop == 0 && $speed_mobile == 0 && $moz == 0){
			$realUrl = str_replace(array("https://", "http://", "www."), "", $realUrl);
			$realUrl = "https://".$realUrl;
			$speed_desktop = getSpeedData($realUrl, "desktop", true, 40, $realUrl);
			$speed_mobile = getSpeedData($realUrl, "mobile", true, 40, $realUrl);
			$moz = domainAuthority($reslUrl);
			$traffic = getTrafficAndEarnings($realUrl);
		}
		
		echo createSite($realUrl, $meta, $speed_desktop, $speed_mobile, $moz, $traffic, $bounce);
	}else{
		echo "Can't fetch website";
	}
}else{
	echo "Invalid domain";
}