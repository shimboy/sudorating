<?php

session_start();
include "connect.php";

function top_rated_sites(){
	global $con;
	$sql = $con->prepare("SELECT * FROM websites");
	$sql->execute();
	$array = array();
	while($row = $sql->fetch(PDO::FETCH_ASSOC)){
		$ratings = $con->prepare("SELECT * FROM reviews WHERE website_id = :id ORDER BY id DESC");
		$ratings->bindParam(":id", $row['id']);
		$ratings->execute();

		$rating = array();
		while($ratings_row = $ratings->fetch(PDO::FETCH_ASSOC)){
			$rating[] = $ratings_row['rating'];
		}

		$average = 0;
		if(count($rating)){ $a = array_filter($rating); $average = array_sum($a)/count($a); }
		$average = number_format($average, 1);
		$array[$row['id']] = $average;
	}
	arsort($array);
	return $array;
}

function has_liked($user_id, $review_id){
	global $con;
	$sql = $con->prepare("SELECT * FROM likes WHERE review_id = :review_id AND user_id = :user_id");
	$sql->bindParam(":review_id", $review_id);
	$sql->bindParam(":user_id", $user_id);
	$sql->execute();
	if ($sql->rowCount() > 0){
		return true;
	}else{
		return false;
	}
}

function like($user_id, $review_id){
	global $con;
	$sql = $con->prepare("SELECT * FROM likes WHERE review_id = :review_id AND user_id = :user_id");
	$sql->bindParam(":review_id", $review_id);
	$sql->bindParam(":user_id", $user_id);
	$sql->execute();
	if ($sql->rowCount() == 0){
		$like = $con->prepare("INSERT INTO likes (review_id, user_id) VALUES(:review_id, :user_id)");
		$like->bindParam(":review_id", $review_id);
		$like->bindParam(":user_id", $user_id);
		$like->execute();
		echo "like";
	}else{
		$unlike = $con->prepare("DELETE FROM likes WHERE review_id = :review_id AND user_id = :user_id");
		$unlike->bindParam(":review_id", $review_id);
		$unlike->bindParam(":user_id", $user_id);
		$unlike->execute();
		echo "unlike";
	}
}

function getMySites($id){
	global $con;
	$sql = $con->prepare("SELECT * FROM websites WHERE owner = :id");
	$sql->bindParam(":id", $id);
	$sql->execute();
	$array = array();
	while($row = $sql->fetch(PDO::FETCH_ASSOC)){
		$array[] = $row;
	}
	return $array;
}

function claim_site($user, $id){
	global $con;
	$sql = $con->prepare("UPDATE websites SET claimed = 1, owner = :user WHERE id = :id");
	$sql->bindParam(":user", $user);
	$sql->bindParam(":id", $id);
	$sql->execute();
}

function update_password($id, $password, $password1, $password2){
	global $con;
	$sql = $con->prepare("SELECT * FROM users WHERE id = :id");
	$sql->bindParam(":id", $id);
	$sql->execute();
	$row = $sql->fetch(PDO::FETCH_ASSOC);
	if ($row['password'] == md5($password)){
		$password = md5($password1);
		$update = $con->prepare("UPDATE users SET password = :password WHERE id = :id");
		$update->bindParam(":password", $password);
		$update->bindParam(":id", $id);
		$update->execute();
		echo "success";
	}else{
		echo "Current password is incorrect, please try again";
	}
}

function update_details($id, $name, $email, $country, $gender){
	global $con;
	$sql = $con->prepare("UPDATE users SET name = :name, email = :email, country = :country, gender = :gender WHERE id = :id");
	$sql->bindParam(":name", $name);
	$sql->bindParam(":email", $email);
	$sql->bindParam(":country", $country);
	$sql->bindParam(":gender", $gender);
	$sql->bindParam(":id", $id);
	$sql->execute();
}

function delete_review($id){
	global $con;
	$is_owner = $con->prepare("SELECT * FROM reviews WHERE id = :id");
	$is_owner->bindParam(":id", $id);
	$is_owner->execute();
	$owner = $is_owner->fetch(PDO::FETCH_ASSOC);
	if ($owner['user_id'] == $_SESSION['id']){
		$sql = $con->prepare("DELETE FROM reviews WHERE id = :id");
		$sql->bindParam(":id", $id);
		$sql->execute();
		return "success";
	}else{
		return "error";
	}
}

function getUserReviews($id){
	global $con;
	$sql = $con->prepare("SELECT * FROM reviews WHERE user_id = :id ORDER BY id DESC");
	$sql->bindParam(":id", $id);
	$sql->execute();
	
	$array = array();
	while($row = $sql->fetch(PDO::FETCH_ASSOC)){
		$array[] = $row;
	}
	return $array;
}

function getReviewCount($id){
	global $con;
	$sql = $con->prepare("SELECT id FROM reviews WHERE user_id = :id");
	$sql->bindParam(":id", $id);
	$sql->execute();
	echo $sql->rowCount();
}

function getReadCount($id){
	global $con;
	$sql = $con->prepare("SELECT id FROM reads WHERE user_id = :id");
	$sql->bindParam(":id", $id);
	$sql->execute();
	echo $sql->rowCount();
}

function getLikeCount($id){
	global $con;
	$sql = $con->prepare("SELECT id FROM likes WHERE user_id = :id");
	$sql->bindParam(":id", $id);
	$sql->execute();
	echo $sql->rowCount();
}

function getProfileData($id){
	global $con;
	$sql = $con->prepare("SELECT * FROM users WHERE id = :id");
	$sql->bindParam(":id", $id);
	$sql->execute();
	return $sql->fetch(PDO::FETCH_ASSOC);
}

function try_login($email, $password){
	global $con;
	$sql = $con->prepare("SELECT * FROM users WHERE email = :email");
	$sql->bindParam(":email", $email);
	$sql->execute();
	if ($sql->rowCount() > 0){
		$row = $sql->fetch(PDO::FETCH_ASSOC);
		if ($row['password'] == md5($password)){
			$_SESSION['id'] = $row['id'];
			$_SESSION['login'] = true;
			$_SESSION['name'] = $row['name'];
			$_SESSION['email'] = $email;
			$_SESSION['gravatar'] = $row['gravatar'];
			echo "success";
		}else{
			echo "error";
		}
	}else{
		echo "error";
	}
}

function create_user($name, $email, $password, $country, $date, $ip){
	global $con;
	
	$does_user_exist = $con->prepare("SELECT email FROM users WHERE email = :email");
	$does_user_exist->bindParam(":email", $email);
	$does_user_exist->execute();
	$verification_code = substr(md5(uniqid(mt_rand(), true)), 0, 10);
	if ($does_user_exist->rowCount() == 0){
		$default = "https://www.kbl.co.uk/wp-content/uploads/2017/11/Default-Profile-Male.jpg";
		$size = 100;
		$grav_url = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;

		$sql = $con->prepare("INSERT INTO users (name, email, password, country, date, ip, gravatar, verification_code)
		VALUES(:name, :email, :password, :country, :date, :ip, :gravatar, :verification_code)");
		$sql->bindParam(":name", $name);
		$sql->bindParam(":email", $email);
		$sql->bindParam(":password", $password);
		$sql->bindParam(":country", $country);
		$sql->bindParam(":date", $date);
		$sql->bindParam(":ip", $ip);
		$sql->bindParam(":gravatar", $grav_url);
		$sql->bindParam(":verification_code", $verification_code);
		$sql->execute();
		$_SESSION['id'] = $con->lastInsertId();
		$_SESSION['login'] = true;
		$_SESSION['name'] = $name;
		$_SESSION['email'] = $email;
		$_SESSION['gravatar'] = $grav_url;
		$to = $email;
		$subject = "Sudoseo - Account Verification";
		$txt = "Thank you for joining Sudoseo! Please click the link below to verify your account<br><br><a href='https://www.sudoseo.com/verify.php?code=".$verification_code."'>https://www.sudoseo.com/verify.php?code='".$verification_code."'</a>";
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: <noreply@sudoseo.com>' . "\r\n";

		mail($to,$subject,$txt,$headers);
		echo "success";
	}else{
		echo "exists";
	}
}

function create_user_google($name, $email, $password, $country, $date, $ip){
	global $con;
	
	$does_user_exist = $con->prepare("SELECT * FROM users WHERE email = :email");
	$does_user_exist->bindParam(":email", $email);
	$does_user_exist->execute();
	if ($does_user_exist->rowCount() == 0){
		$default = "https://www.kbl.co.uk/wp-content/uploads/2017/11/Default-Profile-Male.jpg";
		$size = 100;
		$grav_url = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;

		$sql = $con->prepare("INSERT INTO users (name, email, password, country, date, ip, gravatar)
		VALUES(:name, :email, :password, :country, :date, :ip, :gravatar)");
		$sql->bindParam(":name", $name);
		$sql->bindParam(":email", $email);
		$sql->bindParam(":password", $password);
		$sql->bindParam(":country", $country);
		$sql->bindParam(":date", $date);
		$sql->bindParam(":ip", $ip);
		$sql->bindParam(":gravatar", $grav_url);
		$sql->execute();
		$_SESSION['id'] = $con->lastInsertId();
		$_SESSION['login'] = true;
		$_SESSION['name'] = $name;
		$_SESSION['email'] = $email;
		$_SESSION['gravatar'] = $grav_url;
	}else{
		$row = $does_user_exist->fetch(PDO::FETCH_ASSOC);
		$_SESSION['id'] = $row['id'];
		$_SESSION['login'] = true;
		$_SESSION['name'] = $row['name'];
		$_SESSION['email'] = $row['email'];
		$_SESSION['gravatar'] = $row['gravatar'];
	}
}

?>