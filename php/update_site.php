<?php

require_once("functions.php");
$id = $_POST['id'];

$website = getWebsiteData($id);
$url = $website['url'];
$meta = getMetaData($url, $url);

if ($meta != "error"){
	$speed_desktop = getSpeedData($url, "desktop", true, 40, $url);
	$speed_mobile = getSpeedData($url, "mobile", true, 40, $url);
	$moz = domainAuthority($url);
	$traffic = getTrafficAndEarnings($url);
	$bounce = 0;
	updateSite($id, $url, $meta, $speed_desktop, $speed_mobile, $moz, $traffic, $bounce);
}else{
	echo "Can't fetch website";
}