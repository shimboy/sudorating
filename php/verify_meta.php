<?php

require_once("functions.php");

$website = $_POST['website'];
$website = "https://www.sudoseo.com/";

libxml_use_internal_errors(true);
$curl = _curl($website);
$document = new DOMDocument();
$document->loadHTML($curl);
$xpath = new DOMXPath($document);

$contents = $xpath->query('/html/head/meta[@name="sudoseo-verification-id"]/@content');
if ($contents->length > 0){
	echo "success";
}else{
	echo "error";
}