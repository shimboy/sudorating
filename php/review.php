<?php

session_start();
require_once("functions.php");

$title = $_POST['title'];
$review = $_POST['review'];
$website_id = $_POST['website_id'];
$user_id = $_SESSION['id'];
$rating = $_POST['rating'];
$time = date("Y-m-d H:i");

$sql = $con->prepare("INSERT INTO reviews (website_id, user_id, rating, title, review, time) VALUES
					(:website_id, :user_id, :rating, :title, :review, :time)");
$sql->bindParam(":website_id", $website_id);
$sql->bindParam(":user_id", $user_id);
$sql->bindParam(":rating", $rating);
$sql->bindParam(":title", $title);
$sql->bindParam(":review", $review);
$sql->bindParam(":time", $time);
$sql->execute();

echo get_time_ago(strtotime($time));