<?php

session_start();
include "config.php";
require_once("google/vendor/autoload.php");
$client = new Google_Client();
$client->setClientId($google_client_id);
$client->setClientSecret($google_client_secret);
$client->setApplicationName("Sudoseo");
$client->setRedirectUri("https://www.sudoseo.com/google.php");
$client->addScope("https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email");

?>