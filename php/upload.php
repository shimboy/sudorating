<?php

session_start();
require_once("user_functions.php");

$id = $_SESSION['id'];

$target_dir = "user_images/";

$tmp_name = $_FILES['file']['tmp_name'];
$target_file = $target_dir . basename($_FILES['file']['name']);
$uploadOk = 1;

$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

$check = getimagesize($tmp_name);
if($check == false) {
	$uploadOk = 0;
}

if ($uploadOk == 1) {
	if (move_uploaded_file($tmp_name, $target_file)) {
		$image = "https://www.sudoseo.com/php/".$target_file;
		$sql = $con->prepare("UPDATE users SET gravatar = :image WHERE id = :id");
		$sql->bindParam(":image", $image);
		$sql->bindParam(":id", $id);
		$sql->execute();
		$_SESSION['gravatar'] = $image;
		echo $image;
	}
}else{
	echo "error";
}