<?php

require_once("functions.php");

$phrase = $_GET['phrase'];
$sql = $con->prepare("SELECT * FROM websites WHERE name LIKE '%".$phrase."%'");
$sql->execute();

$sites = array();

while($row = $sql->fetch(PDO::FETCH_ASSOC)){
	$sites[] = array("name" => ucwords($row['name']), "type" => $row['name'], "link" => $row['url'], "icon" => $row['screenshot']);
}

echo json_encode($sites);

?>