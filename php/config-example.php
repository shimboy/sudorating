<?php

$con = new PDO('mysql:host=localhost;dbname=rating', 'root', 'root');

$setting1 = $con->prepare("SELECT * FROM settings WHERE name = 'google_api_key'");
$setting1->execute();
$setting1_row = $setting1->fetch(PDO::FETCH_ASSOC);

$setting2 = $con->prepare("SELECT * FROM settings WHERE name = 'moz_access'");
$setting2->execute();
$setting2_row = $setting2->fetch(PDO::FETCH_ASSOC);

$setting3 = $con->prepare("SELECT * FROM settings WHERE name = 'moz_secret'");
$setting3->execute();
$setting3_row = $setting3->fetch(PDO::FETCH_ASSOC);

$setting4 = $con->prepare("SELECT * FROM settings WHERE name = 'google_client_id'");
$setting4->execute();
$setting4_row = $setting4->fetch(PDO::FETCH_ASSOC);

$setting5 = $con->prepare("SELECT * FROM settings WHERE name = 'google_client_secret'");
$setting5->execute();
$setting5_row = $setting5->fetch(PDO::FETCH_ASSOC);

$googleKey = $setting1_row['value'];
$mozAccess = $setting2_row['value'];
$mozSecret = $setting3_row['value'];

$google_client_id = $setting4_row['value'];
$google_client_secret = $setting5_row['value'];

?>