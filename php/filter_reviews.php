<?php

require_once "functions.php";
require_once "user_functions.php";

$array = json_decode($_POST['array'], true);
$website_id = $_POST['website_id'];

if (!empty($array)){
$sql = $con->prepare("SELECT * FROM reviews WHERE website_id = :website_id AND rating IN (". implode(',', $array) .") ORDER BY id DESC");
}else{
$sql = $con->prepare("SELECT * FROM reviews WHERE website_id = :website_id ORDER BY id DESC");
}
$sql->bindParam(":website_id", $website_id);
$sql->execute();

if ($sql->rowCount() > 0){
	while($review = $sql->fetch(PDO::FETCH_ASSOC)){
	$id = $review['user_id'];
		$user = $con->prepare("SELECT * FROM users WHERE id = :id");
		$user->bindParam(":id", $id);
		$user->execute();
		$ur = $user->fetch(PDO::FETCH_ASSOC);
?>
<div style="width:100%" class="website_top_box container mt-sm-3">
	<div class="row">
		<div class="col-12">
			<div style="display:flex;align-items:center">
			<div style="float:left">
				<img src="<?php echo $ur['gravatar']; ?>" class="rounded-circle" alt="Cinque Terre" style="border:1px solid #e2e2e2;width:45px;height:45px;margin-right:10px">
			</div>
			<div style="float:left">
				<div><?php echo $ur['name']; ?></div>
			</div>
			</div>
			<div class="clearfix"></div>
			<hr>
			<div class="row">
				<div class="col-6">
					<div style="margin-bottom:5px;margin-top:5px" class="form-group">
						<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
						  <i class="fa fa-star"></i>
						</button>
						<button type="button" class="btn btn-sm <?php if ($review['rating'] > 1){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
						  <i class="fa fa-star"></i>
						</button>
						<button type="button" class="btn btn-sm <?php if ($review['rating'] > 2){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
						  <i class="fa fa-star"></i>
						</button>
						<button type="button" class="btn btn-sm <?php if ($review['rating'] > 3){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
						  <i class="fa fa-star"></i>
						</button>
						<button type="button" class="btn btn-sm <?php if ($review['rating'] > 4){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
						  <i class="fa fa-star"></i>
						</button>
					</div>
				</div>
				<div class="col-6 text-right">
					<span style="color:#777"><?php echo get_time_ago(strtotime($review['time'])); ?></span>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-12">
					<div style="font-size:16px;font-weight:500"><?php echo $review['title']; ?></div>
					<div class="mt-sm-2" style="font-size:14px"><?php echo $review['review']; ?></div>
					
				</div>
			</div>						
			<hr>					
			<div class="row">
				<div class="col-12">
					<div style="float:left;margin-right:20px">
					<a style="text-decoration:none" data-id="<?php echo $review['id']; ?>" class="edit like" href="" id="like_<?php echo $review['id']; ?>">
						<?php if (!has_liked($_SESSION['id'], $review['id'])){ ?>
							<i class="fa fa-thumbs-up"></i> Like
						<?php }else{ ?>
							<span style='color:blue'><i class='fa fa-thumbs-up'></i> Liked</span>
						<?php } ?>
					</a>
					</div>
					<div style="float:left;margin-right:20px">
					<a style="text-decoration:none" class="edit" href=""><i class="fa fa-share-alt"></i> Share</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	}
}
?>

