<?php

require_once("functions.php");

$title = $_POST['title'];
$review = $_POST['review'];
$id = $_POST['id'];
$rating = $_POST['rating'];

$sql = $con->prepare("UPDATE reviews SET title = :title, review = :review, rating = :rating WHERE id = :id");
$sql->bindParam(":title", $title);
$sql->bindParam(":review", $review);
$sql->bindParam(":rating", $rating);
$sql->bindParam(":id", $id);
$sql->execute();
