<?php

require_once("connect.php");
include "config.php";

function get_time_ago( $time )
{
    $time_difference = time() - $time;

    if( $time_difference < 1 ) { return 'less than 1 second ago'; }
    $condition = array( 12 * 30 * 24 * 60 * 60 =>  'year',
                30 * 24 * 60 * 60       =>  'month',
                24 * 60 * 60            =>  'day',
                60 * 60                 =>  'hour',
                60                      =>  'minute',
                1                       =>  'second'
    );

    foreach( $condition as $secs => $str )
    {
        $d = $time_difference / $secs;

        if( $d >= 1 )
        {
            $t = round( $d );
            return $t . ' ' . $str . ( $t > 1 ? 's' : '' ) . ' ago';
        }
    }
}

function updateSite($id, $url, $meta, $speed_desktop, $speed_mobile, $moz, $traffic, $bounce){
	global $con;
	$real_url = $url;
	
	$website = getWebsiteData($id);
	$name = $website['name'];
	$added = date("Y-m-d");
	
	$speedd = 0;
	$speedm = 0;
	$usability = 0;
	$domain_authority = 0;
	$page_authority = 0;
	$moz_rank = 0;
	
	if ($speed_desktop != 0){
		$speedd = $speed_desktop["score"]->score;
	}
	
	if ($speed_mobile != 0){
		$speedm = $speed_mobile["score"]->score;
		$usability = $speed_mobile["usability"]->score;
	}
	
	if ($moz != 0){
		$domain_authority = $moz["da"];
		$page_authority = $moz["pa"];
		$moz_rank = $moz["mr"];
	}
	
	$daily_unique = $traffic["uniqueViewsDaily"];
	$daily_page = $traffic["pageViewsDaily"];
	$daily_rev = $traffic["earningsDaily"];
	$monthly_unique = $traffic["uniqueViewsMonthly"];
	$monthly_page = $traffic["pageViewsMonthly"];
	$monthly_rev = $traffic["earningsMonthly"];
	$yearly_unique = $traffic["uniqueViewsYearly"];
	$yearly_page = $traffic["pageViewsYearly"];
	$yearly_rev = $traffic["earningsYearly"];
	$meta = json_encode($meta);
	$screenshot = $speed_desktop["screenshot"];
	$update_date = date("Y-m-d H:i");
	
	$sql = $con->prepare("UPDATE websites SET url = :url, name = :name, added = :added, speed_desktop = :speed_desktop, speed_mobile = :speed_mobile,
	usability = :usability, domain_authority = :domain_authority, page_authority = :page_authority, moz_rank = :moz_rank, bounce_rate = :bounce_rate, daily_unique = :daily_unique,
	daily_page = :daily_page, daily_rev = :daily_rev, monthly_unique = :monthly_unique, monthly_page = :monthly_page, monthly_rev = :monthly_rev, yearly_unique = :yearly_unique,
	yearly_page = :yearly_page, yearly_rev = :yearly_rev, meta = :meta, screenshot = :screenshot, last_update = :last_update WHERE id = :id");
	
	$sql->bindParam(":url", $real_url);
	$sql->bindParam(":name", $name);
	$sql->bindParam(":added", $added);
	$sql->bindParam(":speed_desktop", $speedd);
	$sql->bindParam(":speed_mobile", $speedm);
	$sql->bindParam(":usability", $usability);
	$sql->bindParam(":domain_authority", $domain_authority);
	$sql->bindParam(":page_authority", $page_authority);
	$sql->bindParam(":moz_rank", $moz_rank);
	$sql->bindParam(":bounce_rate", $bounce);
	$sql->bindParam(":daily_unique", $daily_unique);
	$sql->bindParam(":daily_page", $daily_page);
	$sql->bindParam(":daily_rev", $daily_rev);
	$sql->bindParam(":monthly_unique", $monthly_unique);
	$sql->bindParam(":monthly_page", $monthly_page);
	$sql->bindParam(":monthly_rev", $monthly_rev);
	$sql->bindParam(":yearly_unique", $yearly_unique);
	$sql->bindParam(":yearly_page", $yearly_page);
	$sql->bindParam(":yearly_rev", $yearly_rev);
	$sql->bindParam(":meta", $meta);
	$sql->bindParam(":screenshot", $screenshot);
	$sql->bindParam(":last_update", $update_date);
	$sql->bindParam(":id", $id);
	$sql->execute();
}

function giveHost($host_with_subdomain) {
    $array = explode(".", $host_with_subdomain);

    return (array_key_exists(count($array) - 2, $array) ? $array[count($array) - 2] : "").".".$array[count($array) - 1];
}

function createSite($url, $meta, $speed_desktop, $speed_mobile, $moz, $traffic, $bounce){
	global $con;
	$real_url = $url;
	$name = preg_replace('#((?:https?://)?[^/]*)(?:/.*)?$#', '$1', $real_url);
	$name = str_replace(array("https://", "http://", "www."), "", $name);
	$name = giveHost($name);
	$added = date("Y-m-d");
	
	$speedd = 0;
	$speedm = 0;
	$usability = 0;
	$domain_authority = 0;
	$page_authority = 0;
	$moz_rank = 0;
	
	if ($speed_desktop != 0){
		$speedd = $speed_desktop["score"]->score;
	}
	
	if ($speed_mobile != 0){
		$speedm = $speed_mobile["score"]->score;
		$usability = $speed_mobile["usability"]->score;
	}
	
	if ($moz != 0){
		$domain_authority = $moz["da"];
		$page_authority = $moz["pa"];
		$moz_rank = $moz["mr"];
	}
	
	$daily_unique = $traffic["uniqueViewsDaily"];
	$daily_page = $traffic["pageViewsDaily"];
	$daily_rev = $traffic["earningsDaily"];
	$monthly_unique = $traffic["uniqueViewsMonthly"];
	$monthly_page = $traffic["pageViewsMonthly"];
	$monthly_rev = $traffic["earningsMonthly"];
	$yearly_unique = $traffic["uniqueViewsYearly"];
	$yearly_page = $traffic["pageViewsYearly"];
	$yearly_rev = $traffic["earningsYearly"];
	$meta = json_encode($meta);
	$screenshot = $speed_desktop["screenshot"];
	
	if ($screenshot == "")
		$screenshot = "https://www.sudoseo.com/img/error.png";
	
	$sql = $con->prepare("INSERT INTO websites (url, name, added, speed_desktop, speed_mobile,
	usability, domain_authority, page_authority, moz_rank, bounce_rate, daily_unique,
	daily_page, daily_rev, monthly_unique, monthly_page, monthly_rev, yearly_unique,
	yearly_page, yearly_rev, meta, screenshot) VALUES(:url, :name, :added, :speed_desktop, :speed_mobile,
	:usability, :domain_authority, :page_authority, :moz_rank, :bounce_rate, :daily_unique,
	:daily_page, :daily_rev, :monthly_unique, :monthly_page, :monthly_rev, :yearly_unique,
	:yearly_page, :yearly_rev, :meta, :screenshot)");
	
	$sql->bindParam(":url", $real_url);
	$sql->bindParam(":name", $name);
	$sql->bindParam(":added", $added);
	$sql->bindParam(":speed_desktop", $speedd);
	$sql->bindParam(":speed_mobile", $speedm);
	$sql->bindParam(":usability", $usability);
	$sql->bindParam(":domain_authority", $domain_authority);
	$sql->bindParam(":page_authority", $page_authority);
	$sql->bindParam(":moz_rank", $moz_rank);
	$sql->bindParam(":bounce_rate", $bounce);
	$sql->bindParam(":daily_unique", $daily_unique);
	$sql->bindParam(":daily_page", $daily_page);
	$sql->bindParam(":daily_rev", $daily_rev);
	$sql->bindParam(":monthly_unique", $monthly_unique);
	$sql->bindParam(":monthly_page", $monthly_page);
	$sql->bindParam(":monthly_rev", $monthly_rev);
	$sql->bindParam(":yearly_unique", $yearly_unique);
	$sql->bindParam(":yearly_page", $yearly_page);
	$sql->bindParam(":yearly_rev", $yearly_rev);
	$sql->bindParam(":meta", $meta);
	$sql->bindParam(":screenshot", $screenshot);
	$sql->execute();
	return $name;
}

function getWebsiteData($id){
	global $con;
	$array = array();
	$sql = $con->prepare("SELECT * FROM websites WHERE id = :id");
	$sql->bindParam(":id", $id);
	$sql->execute();
	return $sql->fetch(PDO::FETCH_ASSOC);
}

function getWebsites(){
	global $con;
	$array = array();
	$sql = $con->prepare("SELECT * FROM websites ORDER BY RAND() LIMIT 9");
	$sql->execute();
	while($row = $sql->fetch(PDO::FETCH_ASSOC)){
		$array[] = $row;
	}
	return $array;
}

function websiteInDB($id){
	global $con;
	$sql = $con->prepare("SELECT * FROM websites WHERE id = :id");
	$sql->bindParam(":id", $id);
	$sql->execute();
	if ($sql->rowCount() > 0){
		return true;
	}else{
		return false;
	}
}

function getTrafficAndEarnings($url){
	libxml_use_internal_errors(true);
	$html_alexa = _curl('http://www.alexa.com/siteinfo/' . $url);
	$document_alexa = new DOMDocument();
	$document_alexa->loadHTML($html_alexa);
	$selector_alexa = new DOMXPath($document_alexa);
	$rank = 0;
	
	if ($selector_alexa->query("//div[@id='countrydropdown']/ul/li/span")->length > 0){
		$rank = $selector_alexa->query("//div[@id='countrydropdown']/ul/li/span");
		$rank = str_replace(array("#", ","), "", $rank->item(0)->textContent);
		$rank = trim($rank);
	}
	
	$uniqueViewsDaily = (int)(pow($rank,-0.732)*6000000);
	$pageViewsDaily = (int)($uniqueViewsDaily*1.85);
	$uniqueViewsMonthly = $uniqueViewsDaily*28;
	$pageViewsMonthly = $pageViewsDaily*28.5;
	$uniqueViewsYearly = ($uniqueViewsDaily * 27) * 12;
	$pageViewsYearly = ($pageViewsDaily * 28) * 12;
	
	$earningsDaily = ($uniqueViewsDaily*0.017)*0.07;
	if ($rank <= 1000){ $earningsDaily = $earningsDaily * 1.5; }
	if ($rank <= 100){ $earningsDaily = $earningsDaily * 2; }
	
	$earningsMonthly = $earningsDaily * 25;
	$earningsYearly = ($earningsDaily * 22) * 12;
	
	return array("rank" => $rank, "uniqueViewsDaily" => number_format($uniqueViewsDaily), "pageViewsDaily" => number_format($pageViewsDaily), "earningsDaily" => number_format($earningsDaily), "uniqueViewsMonthly" => number_format($uniqueViewsMonthly), "pageViewsMonthly" => number_format($pageViewsMonthly), "earningsMonthly" => number_format($earningsMonthly), "uniqueViewsYearly" => number_format($uniqueViewsYearly), "pageViewsYearly" => number_format($pageViewsYearly), "earningsYearly" => number_format($earningsYearly));
}

function getAlexaBounceRate($url) {
	libxml_use_internal_errors(true);
	$html_alexa = _curl('http://www.alexa.com/siteinfo/' . $url);
	$document_alexa = new DOMDocument();
	$document_alexa->loadHTML($html_alexa);
	$selector_alexa = new DOMXPath($document_alexa);
	
	$bounce = $selector_alexa->query("//span[@class='num purple']");
	echo $bounce->item(0)->textContent;
}

function getMetaData($url, $effectiveUrl){
	$domain = $effectiveUrl;
	if(substr($domain, -1) == '/') $domain = substr($domain,0,-1);
	
	if(!$domain || $domain == '://') $domain = $url;
	$save['body'] = (_curl($domain));
	
	if ($save['body'] != ""){
		$save['charset'] = getCharset($save['body']);
		
		if(mb_strtolower($save['charset']) != 'utf-8' && $save['charset'] != '')
		$save['body'] = iconv($save['charset'],'UTF-8',$save['body']);

		$save['headers'] = _curl_headers($domain);
		
		$save['metaTitle'] 			= getMeta($save['body'],"title");
		$save['metaDescription'] 	= getMeta($save['body'],"description");
		$save['metaKeywords'] 		= getMeta($save['body'],"keywords");
		$save['metaH1'] 			= mb_substr_count($save['body'], "<h1");
		$save['metaH2'] 			= mb_substr_count($save['body'], "<h2");
		$save['metaH3'] 			= mb_substr_count($save['body'], "<h3");
		$save['metaH4'] 			= mb_substr_count($save['body'], "<h4");
		$save['favicon'] 			= getFavIcon($save['body']);				
		$save["robots"] 			= remote_file_exists($domain."/robots.txt");
		$save["sitemap"] 			= remote_file_exists($domain."/sitemap.xml");
		$save['url_real'] 			= $domain;
		return $save;
	}else{
		return "error";
	}
}

function getSpeedData($url,$strategy='desktop',$rules =false,$timeout = 40,$effectiveUrl) {
	global $googleKey;
	$key = trim($googleKey);
	
	$domain = $effectiveUrl;
	if(substr($domain, -1) == '/') $domain = substr($domain,0,-1);
	if(!$domain || $domain == '://') $domain = $url;
	
	$domain = urlencode($domain);
	$lang = 'en';
	$ip = $_SERVER['REMOTE_ADDR'];
	$contents = _curl("https://www.googleapis.com/pagespeedonline/v2/runPagespeed?userIp=$ip&locale=$lang&key=$key&screenshot=true&strategy=$strategy&url=$domain",false,false,false,false,false,$timeout);  
	
	$json 	= json_decode($contents);	
	if($json->responseCode == 200 || $json->responseCode == 404 || $json->responseCode == 403){
		if(!$rules){
			return $json->ruleGroups->SPEED;
		}else{
			return array("score" => $json->ruleGroups->SPEED, "usability" => $json->ruleGroups->USABILITY, "screenshot" => "data:".$json->screenshot->mime_type.";base64, ".str_ireplace(array("_","-"), array("/","+"), $json->screenshot->data));
		}
	}else{
		return 0;
	}
}

function domainAuthority($domain){
	global $mozAccess;
	global $mozSecret;
	$accessID = $mozAccess;
	$secretKey = $mozSecret;
	if(!$accessID || !$secretKey) return 0;
	$expires = time() + 300;
	$stringToSign = $accessID."\n".$expires;
	$binarySignature = hash_hmac('sha1', $stringToSign, $secretKey, true);
	$urlSafeSignature = urlencode(base64_encode($binarySignature));
	$objectURL = $domain;
	$cols = "103079231492";
	$requestUrl = "http://lsapi.seomoz.com/linkscape/url-metrics/".urlencode($objectURL)."?Cols=".$cols."&AccessID=".$accessID."&Expires=".$expires."&Signature=".$urlSafeSignature;

	$options = array(CURLOPT_RETURNTRANSFER => true);

	$ch = curl_init($requestUrl);
	curl_setopt_array($ch, $options);
	$moz = json_decode(curl_exec($ch));
	
	$da = 0;
	$pa = 0;
	$mr = 0;
	curl_close($ch);
	if(!$moz->status){ 
		$da = intval($moz->pda); 
		$pa = intval($moz->upa); 
		$mr = ($moz->umrp);
        return array("da" => $da, "pa" => $pa, "mr" => $mr);		
	}else{
		return 0;
	}
}

function is_domain_valid($url){
	$url = trim($url);
	$url = str_ireplace("\n", "",$url);
	$url = str_ireplace("\r", "",$url);
	
	if(preg_match("#https?://#", $url) === 0) $url = 'http://' . $url;

	$data 	= parse_url($url);
	$domain = $data['host'];
	
	$domain = str_ireplace("www.", "",$domain);
	$domain = mb_strtolower($domain);
	$domain_curl = "http://".$domain;
	if($data['scheme']) $domain_curl = $data['scheme']."://".$domain;
	
	if(is_valid_domain_name($domain_curl)){
		$ret = ping($domain_curl,30);
		if($ret !== 404 && $ret !== 500 && $ret !== 403){
			return true;
		}else{
			return false;
		}
	}else{		
		return false;
	}
}

function is_valid_domain_name($url){
	return filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED) && (preg_match("#^http(s)?://[a-z0-9-_.]+\.[a-z]{2,4}#i", $url));
}

function ping($url,$timeout = 15){
    if(intval($timeout)<3)
        $timeout = 8;

    $headers[] = 'Accept-Language: en';
    
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_setopt($ch,CURLOPT_USERAGENT,"Opera/9.80 (J2ME/MIDP; Opera Mini/4.2.14912/870; U; id) Presto/2.4.15");
    curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);    
    curl_setopt($ch,CURLOPT_TIMEOUT, $timeout); 
    curl_setopt($ch,CURLOPT_FOLLOWLOCATION,false); 
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_MAX_RECV_SPEED_LARGE, 100000);

    curl_exec($ch);
    $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    
    curl_close($ch);
    
    return $retcode;
}

function getWebsiteId($url){
	global $con;
	$plain = str_replace(array("http://", "https://", "www.", "https://www.", "http://www."), "", $url);
	$plain = substr($plain, 0, strrpos($plain, '/'));
	$sql = $con->prepare("SELECT * FROM websites WHERE url LIKE '%".$url."%' OR url LIKE '%".$plain."%'");
	$sql->execute();
	$row = $sql->fetch(PDO::FETCH_ASSOC);
	return $row['id'];
}

function getWebsiteName($url){
	global $con;
	$plain = str_replace(array("http://", "https://", "www.", "https://www.", "http://www."), "", $url);
	$plain = substr($plain, 0, strrpos($plain, '/'));
	$sql = $con->prepare("SELECT * FROM websites WHERE url LIKE '%".$url."%' OR url LIKE '%".$plain."%'");
	$sql->execute();
	$row = $sql->fetch(PDO::FETCH_ASSOC);
	return $row['name'];
}

function website_exists($url){
	global $con;
	$plain = str_replace(array("http://", "https://", "www.", "https://www.", "http://www."), "", $url);
	if (strpos($plain, '/') !== false) {
	$plain = substr($plain, 0, strrpos($plain, '/'));
	}
	$sql = $con->prepare("SELECT * FROM websites WHERE url LIKE '%".$url."%' OR url LIKE '%".$plain."%'");
	$sql->execute();
	if ($sql->rowCount() > 0){
		return true;
	}else{
		return false;
	}
}

function getEfectiveUrl($url, $curl_loops = 0)
{

    if(!ini_get('open_basedir'))
    {
       
        $ch = curl_init();      
        curl_setopt($ch, CURLOPT_URL, $url);   
        curl_setopt($ch,CURLOPT_USERAGENT,"Opera/9.80 (J2ME/MIDP; Opera Mini/4.2.14912/870; U; id) Presto/2.4.15");
        curl_setopt($ch,CURLOPT_FOLLOWLOCATION,1); 
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        $info = curl_getinfo($ch);

        curl_close($ch);
        if($info['url'])      
            return $info['url'];
        else
            return $url;    
    }
    
    return get_final_url($url);

    if( $curl_loops >4)
        return $url;

    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_URL, $url); 
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FORBID_REUSE, false);
    curl_setopt($ch,CURLOPT_USERAGENT,"Opera/9.80 (J2ME/MIDP; Opera Mini/4.2.14912/870; U; id) Presto/2.4.15");
    $data = curl_exec($ch);
 
  
    list($header, $data) = explode("\n\n", $data, 2);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);
    
      
    if ($http_code == 301 || $http_code == 302 || $http_code == 307)
    {
            $matches = array();
            preg_match('/Location:(.*?)\n/', $header, $matches);
            $url = @parse_url(trim(array_pop($matches)));
            
            if (!$url)
            {
                //couldn't process the url to redirect to
                $curl_loops = 0;
                return false;
            }
            
            $last_url = parse_url(curl_getinfo($ch, CURLINFO_EFFECTIVE_URL));
            
            $curl_loops++;
            if (!$url['scheme'])
                $url['scheme'] = $last_url['scheme'];
            if (!$url['host'])
                $url['host'] = $last_url['host'];
            if (!$url['path'])
                $url['path'] = $last_url['path'];
            
            if(substr($url_temp, -1) != "/")
                $url_temp = $url_temp."/";
             if(substr($url['path'], -1) == "/")
                $url['path'] = substr($url['path'],0,-1);
             if(! $url['host'])
                $new_url = $url_temp . $url['path'] . ($url['query']?'?'.$url['query']:'');
            else
                $new_url = $url['scheme'] . '://' . $url['host'] . $url['path'] . ($url['query']?'?'.$url['query']:'');
            
            return getEfectiveUrl($new_url, $curl_loops);
    } else {
        $curl_loops=0;
        return $url;
    }
}

function get_final_url($url){
    $redirects = get_all_redirects($url);
    if (count($redirects)>0){
        return array_pop($redirects);
    } else {
        return $url;
    }
}

function get_all_redirects($url){
    $redirects = array();
    while ($newurl = get_redirect_url($url)){
        if (in_array($newurl, $redirects)){
            break;
        }
        $redirects[] = $newurl;
        $url = $newurl;
    }
    return $redirects;
}

function get_redirect_url($url){
    $redirect_url = null; 

    $url_parts = @parse_url($url);
    if (!$url_parts) return false;
    if (!isset($url_parts['host'])) return false; //can't process relative URLs
    if (!isset($url_parts['path'])) $url_parts['path'] = '/';

    $sock = fsockopen($url_parts['host'], (isset($url_parts['port']) ? (int)$url_parts['port'] : 80), $errno, $errstr, 30);
    if (!$sock) return false;

    $request = "HEAD " . $url_parts['path'] . (isset($url_parts['query']) ? '?'.$url_parts['query'] : '') . " HTTP/1.1\r\n"; 
    $request .= 'Host: ' . $url_parts['host'] . "\r\n"; 
    $request .= "Connection: Close\r\n\r\n"; 
    fwrite($sock, $request);
    $response = '';
    while(!feof($sock)) $response .= fread($sock, 8192);
    fclose($sock);

    if (preg_match('/^Location: (.+?)$/m', $response, $matches)){
        if ( substr($matches[1], 0, 1) == "/" )
            return $url_parts['scheme'] . "://" . $url_parts['host'] . trim($matches[1]);
        else
            return trim($matches[1]);

    } else {
        return false;
    }

}

function getCharset($html)
{
    $r = "/charset=\"(.+?)\"/";    
    preg_match($r,$html,$charset_a);
    if($charset_a[1])
        $charset = $charset_a[1];
    else
    {
        $r = '@content="([\\w/]+)(;\\s+charset=([^\\s"]+))?@i';
        preg_match($r,$html,$charset_a);
        
        if($charset_a[3])
            $charset = $charset_a[3];
    }
    return $charset;
}

function getMeta($html,$tag)
{
   if($tag == 'title')
    {
        return get_title($html);
    }
    else
    {
        $meta = getMetaTags($html);
        foreach ($meta as $key => $value) {
             if(mb_strtolower($key) == mb_strtolower($tag))
             {
                return strip_tags($value);
             }
        }
    }
    return "";
}

function getMetaTags($str)
{
  $pattern = '
  ~<\s*meta\s

  # using lookahead to capture type to $1
    (?=[^>]*?
    \b(?:name|property|http-equiv)\s*=\s*
    (?|"\s*([^"]*?)\s*"|\'\s*([^\']*?)\s*\'|
    ([^"\'>]*?)(?=\s*/?\s*>|\s\w+\s*=))
  )

  # capture content to $2
  [^>]*?\bcontent\s*=\s*
    (?|"\s*([^"]*?)\s*"|\'\s*([^\']*?)\s*\'|
    ([^"\'>]*?)(?=\s*/?\s*>|\s\w+\s*=))
  [^>]*>

  ~ix';
  
  if(preg_match_all($pattern, $str, $out))
    return array_combine($out[1], $out[2]);
  return array();
}

function get_title($str){
    $str = mb_strtoupper($str);
    if(!$str) return "";
	libxml_use_internal_errors(true);
    $doc = new DOMDocument();
    $doc->loadHTML(mb_convert_encoding($str, 'HTML-ENTITIES', 'UTF-8'));

    $nodes = $doc->getElementsByTagName('title');
    
    return $nodes->item(0)->nodeValue;
}

function getFavIcon($html)
{	
	if($html == '')
		return false;
		
		libxml_use_internal_errors(true);
		$doc = new DOMDocument();	
		if(!$doc->loadHTML($html))
			return false;
		
		
		$xml = simplexml_import_dom($doc);
		if(!$xml)
			return false;
		$arr = $xml->xpath('//link[@rel="shortcut icon"]');
		if(!$arr[0]['href'])
		{
			$arr = $xml->xpath('//link[@rel="icon"]');
		}
		if(!$arr[0]['href'])
		{
			$arr = $xml->xpath('//link[@rel="icon shortcut"]');
		}
		return (String)$arr[0]['href'];
	
}

function remote_file_exists($url)
{
     
    $ch = curl_init($url);
//    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,5); 
    curl_setopt($ch, CURLOPT_TIMEOUT, 5); //timeout in seconds
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_MAX_RECV_SPEED_LARGE, 100000);
    curl_exec($ch);
    $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);   
  
    curl_close($ch);
    if($retcode > 400)
        return 0;
    return 1;
}

function _curl($url,$post = false,$headers = false,$json = false,$put = false,$cert=false,$timeout = 20) {    
    $ch = curl_init(); 
    curl_setopt($ch,CURLOPT_USERAGENT,"Opera/9.80 (J2ME/MIDP; Opera Mini/4.2.14912/870; U; id) Presto/2.4.15");
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);    
    curl_setopt($ch,CURLOPT_TIMEOUT, $timeout); 

    curl_setopt($ch,CURLOPT_ENCODING, ""); 
  
    curl_setopt($ch,CURLOPT_FOLLOWLOCATION,1);  


     
    curl_setopt($ch, CURLOPT_MAX_RECV_SPEED_LARGE, 100000);
    
    $headers[] = 'Accept-Language: en';
    if($post)
    {
        $fields_string  = "";
        if(is_array($post))
        {
            foreach($post as $key => $value)
            {
                $fields_string .= $key."=".$value."&";
            }
            $fields_string          =rtrim ($fields_string,'&');
        }
        else
        {
            $fields_string          = $post;   
        }
        curl_setopt($ch,CURLOPT_POST,count($post));
        if($json)
        {
            $headers[]              = 'Accept: application/json';           
            $headers[]              = 'Content-Type: application/json';         
            if(is_array($post))
                $fields_string = json_encode($post);
            curl_setopt($ch,CURLOPT_POST,1);
        }
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        
        curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
    }
    if(strtolower(parse_url($url, PHP_URL_SCHEME)) == 'https')
    {
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
        if($cert)
        {            
            curl_setopt($ch, CURLOPT_CAINFO, $cert); 
        }
    }
     if($headers)
    {
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    }

    if($put)
    {
        if($put === true)
            $put = "PUT";
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $put);
    }

    curl_setopt($ch, CURLOPT_URL, $url); 
    $data = curl_exec($ch);
    curl_close($ch);
	
    return $data;
}

function _curl_headers($url)
{
   $curl = curl_init();

    $opts = array (
            CURLOPT_URL => $url,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => FALSE,
            CURLOPT_ENCODING => 'gzip',
            CURLOPT_HEADER => true,
            CURLOPT_NOBODY => true,
            CURLOPT_COOKIESESSION, true,
            CURLOPT_MAX_RECV_SPEED_LARGE => 100000,
            CURLOPT_USERAGENT => "Opera/9.80 (J2ME/MIDP; Opera Mini/4.2.14912/870; U; id) Presto/2.4.15"
    );

    curl_setopt_array($curl, $opts);
    $return = curl_exec($curl);
    list($rawHeader, $response) = explode("\r\n\r\n", $return, 2);
    $cutHeaders = explode("\r\n", $rawHeader);
    $headers = array();
    foreach ($cutHeaders as $row)
    {
        $cutRow = explode(":", $row, 2);
        $headers[$cutRow[0]] = trim($cutRow[1]);
    }

    return implode("<br>",$cutHeaders);
}