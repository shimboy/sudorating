<?php

require_once("php/functions.php");
require_once("php/user_functions.php");

$website_id = $_GET['website_id'];
$sql = $con->prepare("SELECT * FROM websites WHERE name = '".$website_id."'");
$sql->execute();
$row = $sql->fetch(PDO::FETCH_ASSOC);
$website_id = $row['id'];

if (!isset($website_id)){ header("Location: index.php"); }
if (websiteInDB($website_id) == false){ header("Location: index.php"); }

$website = getWebsiteData($website_id);
$website_url = $website['url'];
$website_name = str_replace(array("https://", "http://", "www."), "", $website['name']);
if (strpos($website_name, '.') !== false){
$website_name = substr($website_name, 0, strpos($website_name, "."));
}
$website_name = ucwords($website_name);

$meta = json_decode($website['meta'], true);
$meta_desc =  $meta['metaDescription'];

$related = $con->prepare("SELECT * FROM websites WHERE id != :id ORDER BY RAND() LIMIT 5");
$related->bindParam(":id", $website_id);
$related->execute();

$reviews = $con->prepare("SELECT * FROM reviews WHERE website_id = :id ORDER BY id DESC");
$reviews->bindParam(":id", $website_id);
$reviews->execute();

$ratings = $con->prepare("SELECT * FROM reviews WHERE website_id = :id ORDER BY id DESC");
$ratings->bindParam(":id", $website_id);
$ratings->execute();

$r = array();
while($rr = $ratings->fetch(PDO::FETCH_ASSOC)){
	$r[] = $rr['rating'];
}

$average = 0;

if(count($r)) {
    $a = array_filter($r);
    $average = array_sum($a)/count($a);
}

$average = floor($average);

function array_avg($array, $round=1){
    $num = count($array);
    return array_map(
        function($val) use ($num,$round){
            return array('count'=>$val,'avg'=>round($val/$num*100, $round));
        },
        array_count_values($array));
}

$percentage = array_avg($r);

include "header.php";

?>
<meta property="og:url"           content="https://www.sudoseo.com/website/<?php echo $website['name']; ?>" />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="<?php echo $website_name; ?>" />
<meta property="og:description"   content="<?php echo $meta_desc; ?>" />
<meta property="og:image"         content="<?php echo $website['screenshot']; ?>" />
<div class="rp_top_bar">
	<div class="rp_top_bar_container container">
		<div class="row">
			<div class="col-md-8">
				<div class="rp_image_container">
					<img class="rp_image" src="<?php echo $website['screenshot']; ?>">
				</div>
				<div class="rp_title_container">
					<div class="rp_title">
						<h1 style="margin:0"><?php echo ucwords($website['name']); ?></h1>
					</div>
					<div class="rp_reviews">
						Reviews <?php echo $reviews->rowCount(); ?> - 
						<?php if ($average == 1 || $average == 0){ ?>
						Bad
						<?php }else if ($average == 2){ ?>
						Poor
						<?php }else if ($average == 3){ ?>
						Average
						<?php }else if ($average == 4){ ?>
						Great
						<?php }else if ($average == 5){ ?>
						Excellent
						<?php } ?>
					</div>
					<div style="margin-top:10px" class="rp_rating">
						<div style="margin-bottom:5px;margin-top:5px" class="form-group">
						<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
						  <i class="fa fa-star"></i>
						</button>
						<button type="button" class="btn btn-sm <?php if ($average > 1){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
						  <i class="fa fa-star"></i>
						</button>
						<button type="button" class="btn btn-sm <?php if ($average > 2){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
						  <i class="fa fa-star"></i>
						</button>
						<button type="button" class="btn btn-sm <?php if ($average > 3){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
						  <i class="fa fa-star"></i>
						</button>
						<button type="button" class="btn btn-sm <?php if ($average > 4){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
						  <i class="fa fa-star"></i>
						</button>
					</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="badge-card">
					<a class="badge-card__section badge-card__section--hoverable" href="<?php echo $website['url']; ?>" target="_blank" rel="nofollow" data-track-link="{'name': 'visit-company'}">
						<div class="badge-card__header">
							<svg class="icon badge-card__external-link-icon">
								<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="external-link-alt" class="svg-inline--fa fa-external-link-alt fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M576 24v127.984c0 21.461-25.96 31.98-40.971 16.971l-35.707-35.709-243.523 243.523c-9.373 9.373-24.568 9.373-33.941 0l-22.627-22.627c-9.373-9.373-9.373-24.569 0-33.941L442.756 76.676l-35.703-35.705C391.982 25.9 402.656 0 424.024 0H552c13.255 0 24 10.745 24 24zM407.029 270.794l-16 16A23.999 23.999 0 0 0 384 303.765V448H64V128h264a24.003 24.003 0 0 0 16.97-7.029l16-16C376.089 89.851 365.381 64 344 64H48C21.49 64 0 85.49 0 112v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V287.764c0-21.382-25.852-32.09-40.971-16.97z"></path></svg>
							</svg>
							<span class="badge-card__title">
								www.<?php echo $website['name']; ?>
							</span>
						</div>
						<div class="badge-card__description">
							Visit this website
						</div>
					</a>
					<div id="unclaim" class="badge-card__section inviting-status has-tooltip">
						<div class="badge-card__header">
							<?php if ($website['claimed'] == 0){ ?>
							<svg style="margin-right:7px" class="icon badge-card__external-link-icon">
								<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="question-circle" class="svg-inline--fa fa-question-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zM262.655 90c-54.497 0-89.255 22.957-116.549 63.758-3.536 5.286-2.353 12.415 2.715 16.258l34.699 26.31c5.205 3.947 12.621 3.008 16.665-2.122 17.864-22.658 30.113-35.797 57.303-35.797 20.429 0 45.698 13.148 45.698 32.958 0 14.976-12.363 22.667-32.534 33.976C247.128 238.528 216 254.941 216 296v4c0 6.627 5.373 12 12 12h56c6.627 0 12-5.373 12-12v-1.333c0-28.462 83.186-29.647 83.186-106.667 0-58.002-60.165-102-116.531-102zM256 338c-25.365 0-46 20.635-46 46 0 25.364 20.635 46 46 46s46-20.636 46-46c0-25.365-20.635-46-46-46z"></path></svg>
							</svg>
							<?php } ?>
							<span style="margin:0" class="badge-card__title">
								<?php if ($website['claimed'] == 1){?><i style="margin-right:7px" class="fa fa-check-circle"></i><?php } ?><?php if ($website['claimed'] == 0){ ?>Unclaimed<?php }else{?>Claimed<?php } ?>
							</span>
						</div> 
						<div class="badge-card__description">
							<?php if ($website['claimed'] == 0){ ?>This company hasn't been claimed.<?php }else{ ?>This cpmpany has been claimed<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
</div>

<input type="hidden" id="website_id" value="<?php echo $website_id; ?>">

<div class="homepage_background_image">
	<div style="background:none" class="main container-fluid">
		<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="website_top_box container mt-sm-3">
				<div class="row">
					<div class="col-md-12">
						<ul class="rp_tabs nav nav-tabs">
							  <li class="nav-item">
								<a data-toggle="tab" href="#reviews" role="tab" aria-controls="nav-home" aria-selected="true" class="nav-link active" href="#">Reviews</a>
							  </li>
							  <li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#stats" role="tab" aria-controls="nav-home" aria-selected="false">Statistics</a>
							  </li>
						</ul>
					</div>
				</div>
				<br>
					<div class="tab-content">
					<div class="tab-pane fade" id="stats" role="tabpanel">
					<?php 
					$time = strftime('%Y-%m-%d',(strtotime('7 days ago')));
					if($website['last_update'] < $time){ ?>
					<div class="row" style="display:flex;align-items:center">
						<div class="col-md-6">
						<span style="font-weight:500;font-size:1.6em">Site Statistics</span>
						</div>
						<div class="col-md-6 text-right">
							<button id="update" type="submit" class="btn btn-info">Update</button>
						</div>
					</div>
					<hr>
					<?php } ?>
					<div class="row">
						<div class="col-xl-3 col-lg-6 mt-sm-2 mb-sm-2">
							<div class="progress_a mx-auto" data-value='<?php echo $website['speed_desktop']; ?>'>
							  <span class="progress-left">
									<span class="progress-bar"></span>
							  </span>
							  <span class="progress-right">
									<span class="progress-bar"></span>
							  </span>
							  <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
								<div style="text-align:center;color:#a2a2a2;font-size:1.5em"><span class="pagespeedd"></span><div style="color:#777;font-size:0.5em">Speed Desktop</div></div>
							  </div>
							</div>
						</div>
						<div class="col-xl-3 col-lg-6 mt-sm-2 mb-sm-2">
							<div class="progress_a mx-auto" data-value='<?php echo $website['speed_mobile']; ?>'>
							  <span class="progress-left">
									<span class="progress-bar"></span>
							  </span>
							  <span class="progress-right">
									<span class="progress-bar"></span>
							  </span>
							  <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
								<div style="text-align:center;color:#a2a2a2;font-size:1.5em"><span class="pagespeedd"></span><div style="color:#777;font-size:0.5em">Speed Mobile</div></div>
							  </div>
							</div>
						</div>
						<div class="col-xl-3 col-lg-6 mt-sm-2 mb-sm-2">
							<div class="progress_a mx-auto" data-value='<?php echo $website['usability']; ?>'>
							  <span class="progress-left">
									<span class="progress-bar"></span>
							  </span>
							  <span class="progress-right">
									<span class="progress-bar"></span>
							  </span>
							  <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
								<div style="text-align:center;color:#a2a2a2;font-size:1.5em"><span class="pagespeedd"></span><div style="color:#777;font-size:0.5em">Usability Mobile</div></div>
							  </div>
							</div>
						</div>
						<div class="col-xl-3 col-lg-6 mt-sm-2 mb-sm-2">
							<div class="progress_a mx-auto" data-value='<?php echo $website['domain_authority']; ?>'>
							  <span class="progress-left">
									<span class="progress-bar"></span>
							  </span>
							  <span class="progress-right">
									<span class="progress-bar"></span>
							  </span>
							  <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
								<div style="text-align:center;color:#a2a2a2;font-size:1.5em"><span class="pagespeedd"></span><div style="color:#777;font-size:0.5em">Domain Authority</div></div>
							  </div>
							</div>
						</div>
					</div>
					
					<hr style="margin-top:20px;margin-bottom:30px">
					
					<div>
						<div class="col-md-12">
							<div style="border:1px solid #e2e2e2;margin:0 auto">
								<img style="width:100%" src="https://www.sudoseo.com/img/banner.jpg">
							</div>
						</div>
					</div>
					
					<hr style="margin-top:20px;margin-bottom:30px">
					
					<div class="row">
						<div class="col-md-4">
							<div>
								<div style="height:100%;float:left;margin-right:10px"><i class="fa fa-circle text-success"></i></div>
								<div style="float:left">
								<span style="color:#444;font-size:13px;font-weight:500">Page Authority</span><br>
								<span class="small text-muted">Authority <?php echo $website['page_authority']; ?>%</span>
								</div>
							</div>
						</div>
						<div class="col-md-8">
							<div style="height:5px" class="progress mt-sm-2">
							  <div class="progress-bar bg-success" role="progressbar" style="height:5px;width: <?php echo $website['page_authority']; ?>%" aria-valuenow="<?php echo $website['page_authority']; ?>" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
					</div>
					<div class="row mt-sm-4">
						<div class="col-md-4">
							<div>
								<div style="height:100%;float:left;margin-right:10px"><i class="fa fa-circle text-success"></i></div>
								<div style="float:left">
								<span style="color:#444;font-size:13px;font-weight:500">Moz Rank</span><br>
								<span class="small text-muted"><?php echo number_format($website['moz_rank'], 1); ?>/10</span>
								</div>
							</div>
						</div>
						<div class="col-md-8">
							<div style="height:5px" class="progress mt-sm-2">
							  <div class="progress-bar bg-success" role="progressbar" style="height:5px;width: <?php echo number_format($website['moz_rank'] * 10); ?>%" aria-valuenow="<?php echo number_format($website['moz_rank'] * 10); ?>" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
					</div>
					<div class="row mt-sm-4">
						<div class="col-md-4">
							<div>
								<div style="height:100%;float:left;margin-right:10px"><i class="fa fa-circle text-success"></i></div>
								<div style="float:left">
								<span style="color:#444;font-size:13px;font-weight:500">Bounce Rate</span><br>
								<span class="small text-muted">Rate <?php echo $website['bounce_rate']; ?>%</span>
								</div>
							</div>
						</div>
						<div class="col-md-8">
							<div style="height:5px" class="progress mt-sm-2">
							  <div class="progress-bar bg-success" role="progressbar" style="height:5px;width: <?php echo $website['bounce_rate']; ?>%" aria-valuenow="<?php echo $website['bounce_rate']; ?>" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
					</div>
					
					<hr style="margin-top:20px;margin-bottom:30px">
					
					<div class="row">
						<div class="col-sm-12">
							<div style="width:100%;background:#7597eb;color:#fff;padding:10px">
								Daily Traffic Stats
							</div>
						</div>
					</div>
					<div class="row mt-sm-3">
						<div class="col-md-4 text-center">
							<h3 style="color:#444"><?php echo $website['daily_unique']; ?></h3>
							<div class="small">Unique Visits</div>
						</div>
						<div class="col-md-4 text-center">
							<h3 style="color:#444"><?php echo $website['daily_page']; ?></h3>
							<div class="small">Page Views</div>
						</div>
						<div class="col-md-4 text-center">
							<h3 style="color:#444">$<?php echo $website['daily_rev']; ?></h3>
							<div class="small">Revenue</div>
						</div>
					</div>
					<hr>
					
					<div class="row">
						<div class="col-sm-12">
							<div style="width:100%;background:#6abd5e;color:#fff;padding:10px">
								Monthly Traffic Stats
							</div>
						</div>
					</div>
					<div class="row mt-sm-3">
						<div class="col-md-4 text-center">
							<h3 style="color:#444"><?php echo $website['monthly_unique']; ?></h3>
							<div class="small">Unique Visits</div>
						</div>
						<div class="col-md-4 text-center">
							<h3 style="color:#444"><?php echo $website['monthly_page']; ?></h3>
							<div class="small">Page Views</div>
						</div>
						<div class="col-md-4 text-center">
							<h3 style="color:#444">$<?php echo $website['monthly_rev']; ?></h3>
							<div class="small">Revenue</div>
						</div>
					</div>
					<hr>
					
					<div class="row">
						<div class="col-sm-12">
							<div style="width:100%;background:#e0a25a;color:#fff;padding:10px">
								Yearly Traffic Stats
							</div>
						</div>
					</div>
					<div class="row mt-sm-3">
						<div class="col-md-4 text-center">
							<h3 style="color:#444"><?php echo $website['yearly_unique']; ?></h3>
							<div class="small">Unique Visits</div>
						</div>
						<div class="col-md-4 text-center">
							<h3 style="color:#444"><?php echo $website['yearly_page']; ?></h3>
							<div class="small">Page Views</div>
						</div>
						<div class="col-md-4 text-center">
							<h3 style="color:#444">$<?php echo $website['yearly_rev']; ?></h3>
							<div class="small">Revenue</div>
						</div>
					</div>
					</div>
					<div class="tab-pane fade show active" id="reviews" role="tabpanel">
						<div class="row">
							<div class="col-12 text-center">
								<a id="leave_review" data-id="<?php echo $website['id']; ?>" style="color:#4c5d91" href=""><i class="fa fa-edit"></i> Leave a review for <?php echo $website['name']; ?></a>
							</div>
						</div>
						
						<hr style="margin-top:20px;margin-bottom:30px">
						
						<?php if ($reviews->rowCount() > 0){ ?>
						<h4>Reviews &nbsp;<span style="color:#777"></h4>
						<br>
						<div style="color:#555;display:flex;align-items:center" class="row">
							<div class="col-md-2">
									<input class="checkbox" style="margin-right:5px" type="checkbox" class="checkbox" id="5"> Excellent
							</div>
							<div class="col-md-9">
								<div style="height:14px" class="progress">
								  <div class="progress-bar bg-success" role="progressbar" style="height:14px;width: <?php echo $percentage[5]['avg']; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
							</div>
							<div class="col-md-1 text-right">
								<?php echo number_format($percentage[5]['avg']); ?>%
							</div>
						</div>
						
						<div style="color:#555;margin-top:5px;display:flex;align-items:center" class="row">
							<div class="col-md-2">
									<input style="margin-right:5px" type="checkbox" class="checkbox" id="4"> Great
							</div>
							<div class="col-md-9">
								<div style="height:14px" class="progress">
								  <div class="progress-bar bg-success" role="progressbar" style="height:14px;width: <?php echo $percentage[4]['avg']; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
							</div>
							<div class="col-md-1 text-right">
								<?php echo number_format($percentage[4]['avg']); ?>%
							</div>
						</div>
						
						<div style="color:#555;margin-top:5px;display:flex;align-items:center" class="row">
							<div class="col-md-2">
									<input style="margin-right:5px" type="checkbox" class="checkbox" id="3"> Average
							</div>
							<div class="col-md-9">
								<div style="height:14px" class="progress">
								  <div class="progress-bar bg-success" role="progressbar" style="height:14px;width: <?php echo $percentage[3]['avg']; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
							</div>
							<div class="col-md-1 text-right">
								<?php echo number_format($percentage[3]['avg']); ?>%
							</div>
						</div>
						
						<div style="color:#555;margin-top:5px;display:flex;align-items:center" class="row">
							<div class="col-md-2">
									<input style="margin-right:5px" type="checkbox" class="checkbox" id="2"> Poor
							</div>
							<div class="col-md-9">
								<div style="height:14px" class="progress">
								  <div class="progress-bar bg-success" role="progressbar" style="height:14px;width: <?php echo $percentage[2]['avg']; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
							</div>
							<div class="col-md-1 text-right">
								<?php echo number_format($percentage[2]['avg']); ?>%
							</div>
						</div>
						
						<div style="color:#555;margin-top:5px;display:flex;align-items:center" class="row">
							<div class="col-md-2">
									<input style="margin-right:5px" type="checkbox" class="checkbox" id="1"> Bad
							</div>
							<div class="col-md-9">
								<div style="height:14px" class="progress">
								  <div class="progress-bar bg-success" role="progressbar" style="height:14px;width: <?php echo $percentage[1]['avg']; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
							</div>
							<div class="col-md-1 text-right">
								<?php echo number_format($percentage[1]['avg']); ?>%
							</div>
						</div>
						
						<hr style="margin-top:30px;margin-bottom:40px">
						
						<div class="col-md-12">
							<div style="border:1px solid #e2e2e2;margin:0 auto">
								<img style="width:100%" src="https://www.sudoseo.com/img/banner.jpg">
							</div>
						</div>
						
						<hr style="margin-top:30px;margin-bottom:40px">
						<?php } ?>
						
						<div class="results" id="review_list">
						<?php
						if ($reviews->rowCount() > 0){
							while($review = $reviews->fetch(PDO::FETCH_ASSOC)){
								$id = $review['user_id'];
								$user = $con->prepare("SELECT * FROM users WHERE id = :id");
								$user->bindParam(":id", $id);
								$user->execute();
								$ur = $user->fetch(PDO::FETCH_ASSOC);
						?>
						<div style="width:100%" class="review website_top_box container mt-sm-3">
							<div class="row">
								<div class="col-12">
									<div style="display:flex;align-items:center">
									<div style="float:left">
										<img src="<?php echo $ur['gravatar']; ?>" class="rounded-circle" alt="Cinque Terre" style="border:1px solid #e2e2e2;width:45px;height:45px;margin-right:10px">
									</div>
									<div style="float:left">
										<div><?php echo $ur['name']; ?></div>
									</div>
									</div>
									<div class="clearfix"></div>
									<hr>
									<div class="row">
										<div class="col-6">
											<div style="margin-bottom:5px;margin-top:5px" class="form-group">
												<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
												  <i class="fa fa-star"></i>
												</button>
												<button type="button" class="btn btn-sm <?php if ($review['rating'] > 1){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
												  <i class="fa fa-star"></i>
												</button>
												<button type="button" class="btn btn-sm <?php if ($review['rating'] > 2){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
												  <i class="fa fa-star"></i>
												</button>
												<button type="button" class="btn btn-sm <?php if ($review['rating'] > 3){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
												  <i class="fa fa-star"></i>
												</button>
												<button type="button" class="btn btn-sm <?php if ($review['rating'] > 4){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
												  <i class="fa fa-star"></i>
												</button>
											</div>
										</div>
										<div class="col-6 text-right">
											<span style="color:#777"><?php echo get_time_ago(strtotime($review['time'])); ?></span>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-12">
											<div style="font-size:16px;font-weight:500"><?php echo $review['title']; ?></div>
											<div class="mt-sm-2" style="font-size:14px"><?php echo $review['review']; ?></div>
											
										</div>
									</div>						
									<hr>					
									<div class="row">
										<div class="col-12">
											<div style="float:left;margin-right:20px">
											<a style="text-decoration:none" data-id="<?php echo $review['id']; ?>" class="edit like" href="" id="like_<?php echo $review['id']; ?>">
												<?php if (!has_liked($_SESSION['id'], $review['id'])){ ?>
													<i class="fa fa-thumbs-up"></i> Like
												<?php }else{ ?>
													<span style='color:blue'><i class='fa fa-thumbs-up'></i> Liked</span>
												<?php } ?>
											</a>
											</div>
											<div style="float:left;margin-right:20px">
											<a style="text-decoration:none" class="share edit" href="" data-id="<?php echo $review['id']; ?>"><i class="fa fa-share-alt"></i> Share</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php } } ?>
						</div>
						<?php if ($reviews->rowCount() > 0){ ?>
						<br>
						<ul style="justify-content:center" class="pagination">
							<li class="page-item">
								<a id="previous-page" class="page-link" href="javascriot:void(0)" aria-label="Previous">
									<span aria-hidden="true">&laquo;</span>
								</a>
							</li>
						</ul>
						<?php } ?>
					</div>
					</div>
					
				</div>
		
		</div>
		<div class="col-md-4">
			<div class="website_top_box container mt-sm-3">
				<h3>Top Rated Sites</h3>
				<hr>
				<div style="width:100%;background:#f0f0f0;padding:10px;margin-bottom:10px">
					<?php 
					$count = 0;
					foreach(top_rated_sites() as $key => $val){
						$count++;
						if ($count > 10) break;
						$website = getWebsiteData($key);
					?>
					<div style="width:99%;margin:-left aut;margin-right:auto;margin-bottom:10px;background:#fff;padding:10px">
					<div class="row">
						<div class="col-12">
								<div class="row">
									<div class="col-md-10">
										<div style="float:left">
											<a href="<?php echo $website['name']; ?>"><img src="<?php echo $website['screenshot']; ?>" style="margin-right:2px;border:1px solid #e2e2e2;max-height:30px"></a>
										</div>
										<div style="float:left;margin-left:10px">
											<h3 style="font-size:1em"><a style="color:#333" href="<?php echo $website['name']; ?>"><?php 
											$name = ucwords($website['name']);
											echo substr($name, 0, strpos($name, "."));
											?></a></h3>
										</div>
									</div>
									<div class="col-md-2">
										<div class="rating-value"><?php echo $val; ?></div>
									</div>
								</div>
						</div>
					</div>
					</div>
					<?php } ?>
					</div>
			</div>
			
			<div class="website_top_box container mt-sm-3">
				<div class="row">
					<div class="col-md-12">
						<div style="width:120px;height:600px;border:1px solid #e2e2e2;margin:0 auto">
							<img src="https://www.sudoseo.com/img/skyscraper.png">
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		</div>
	<br><br>
	</div>
</div>

<div id="modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Leave a review for <?php echo $website['name']; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div>
			<h3 style="color:#777">Rating</h3>
			<div style="margin-bottom:5px;margin-top:5px" class="form-group">
				<button type="button" class="btn btn-warning btn-sm rateButton star-selected" aria-label="Left Align">
				  <i class="fa fa-star"></i>
				</button>
				<button type="button" class="btn btn-sm rateButton btn-default btn-grey" aria-label="Left Align">
				  <i class="fa fa-star"></i>
				</button>
				<button type="button" class="btn btn-sm rateButton btn-default btn-grey" aria-label="Left Align">
				  <i class="fa fa-star"></i>
				</button>
				<button type="button" class="btn btn-sm rateButton btn-grey btn-default" aria-label="Left Align">
				  <i class="fa fa-star"></i>
				</button>
				<button type="button" class="btn btn-default btn-grey btn-sm rateButton" aria-label="Left Align">
				  <i class="fa fa-star"></i>
				</button>
			</div>
			
			<input type="hidden" id="rating" value="1">
			
			<br>
			
			<h3 style="color:#777">Review title</h3>
			<input style="color:#777" id="title" type="text" class="form-control">
			
			<br>
			
			<h3 style="color:#777">Your review</h3>
			<textarea id="review" style="width:100%;padding:10px;color:#777" rows="10"></textarea>
		</div>
      </div>
      <div class="modal-footer">
        <button id="submit" type="button" class="btn btn-primary">Submit review</button>
      </div>
    </div>
  </div>
</div>

<div id="modal1" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div style="background:#2a384f;color:#fff" class="modal-content">
      <div class="modal-body">
        <div class="text-center">
			<br>
			<div style="font-size:18px;font-weight:500;color:#e2e2e2" id="modal_site"></div><br><br>
			<div id="wait" style="color:#e2e2e2">Fetching data, please wait...</div><br>
			<div style="height:20px;margin-bottom:25px" class="progresss">
			  <div class="indeterminate"></div>
		  </div>
			<div style="font-size:12px;color:#e2e2e2">This may take a few moments while we gather all website information</div>
			<br><br>
		</div>
      </div>
    </div>
  </div>
</div>

<div id="share_modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Share this review</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  <center>
		<div class="a2a_kit a2a_kit_size_32 a2a_default_style" style="display: flex; justify-content: center;">
		<a class="a2a_dd" href="https://www.addtoany.com/share"></a>
		<a class="a2a_button_facebook"></a>
		<a class="a2a_button_twitter"></a>
		<a class="a2a_button_email"></a>
		</div>
		<script async src="https://static.addtoany.com/menu/page.js"></script>
      	</div>
	  </center>
    </div>
  </div>
</div>

<?php include "footer.php"; ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
<script>
$(function() {
  
  $('.share').click(function(e){
	  e.preventDefault();
	  var id = $(this).attr("data-id");
	  $('#share_modal').modal("show");
	  
  });
  
  $('#unclaim').click(function(){
	  <?php if (isset($_SESSION['login'])){ ?>
	  window.location.href = "https://www.sudoseo.com/claim.php?website_id=<?php echo $website_id; ?>";
	  <?php }else{ ?>
	  $('#login').modal("show");
	  <?php } ?>
  });
  
  var results = $('.results .review').length;
	var limitPerPage = 7;
	$('.results .review:gt('+(limitPerPage -1)+')').hide();
	var totalPages = Math.round(results / limitPerPage);
	$('.pagination').append("<li class='page-item current-page active'><a class='page-link' href='javascript:void(0)'>"+1+"</a></li>");
	
	for(var i = 2; i <= totalPages; i++){
		$('.pagination').append("<li class='page-item current-page'><a class='page-link' href='javascript:void(0)'>"+i+"</a></li>");
	}
	
	$('.pagination').append("<li id='next-page' class='page-item'><a class='page-link' href='javascriot:void(0)' aria-label='Next'><span aria-hidden='true'>&raquo;</span></a></li>");
	
	$('.pagination li.current-page').click(function(){
		if ($(this).hasClass("active")){
			return false;
		}else{
			var currentPage = $(this).index();
			$(".pagination li").removeClass("active");
			$(this).addClass("active");
			$('.results .review').hide();
			
			var grandTotal = limitPerPage * currentPage;
			for(var i = grandTotal - limitPerPage; i < grandTotal; i++){
				$('.results .review:eq('+i+')').show();
			}
		}
	});
	
	$('#next-page').on("click", function(){
		var currentPage = $('.pagination li.active').index();
		if (currentPage == totalPages){
			return false;
		}else{
			currentPage++;
			$(".pagination li").removeClass("active");
			$('.results .review').hide();
			
			var grandTotal = limitPerPage * currentPage;
			for(var i = grandTotal - limitPerPage; i < grandTotal; i++){
				$('.results .review:eq('+i+')').show();
			}
			$('.pagination li.current-page:eq('+(currentPage - 1)+')').addClass("active");
		}
	});
	
	$('#previous-page').on("click", function(){
		var currentPage = $('.pagination li.active').index();
		if (currentPage == 1){
			return false;
		}else{
			currentPage--;
			$(".pagination li").removeClass("active");
			$('.results .review').hide();
			
			var grandTotal = limitPerPage * currentPage;
			for(var i = grandTotal - limitPerPage; i < grandTotal; i++){
				$('.results .review:eq('+i+')').show();
			}
			$('.pagination li.current-page:eq('+(currentPage - 1)+')').addClass("active");
		}
	});
	
  $('#leave_review').click(function(e){
	  e.preventDefault();
	  <?php if (isset($_SESSION['login'])){ ?>
	  $('#modal').modal("show");
	  <?php }else{ ?>
	  $('#login').modal("show");
	  <?php } ?>
		  
  });
  
  var array = [];
  $(".checkbox").change(function() {
    if(this.checked) {
		$('.checkbox').each(function(){
			if(this.checked) {
				var id = $(this).attr("id");
				if (!array.includes(id)){
					array.push(id);
				}
			}
		});
		
    }else{
		var id = $(this).attr("id");
		var index = array.indexOf(id);
		if (index > -1) {
		   array.splice(index, 1);
		}
	}
	
	var website_id = '<?php echo $website_id; ?>';
	$.post("https://www.sudoseo.com/php/filter_reviews.php", {website_id:website_id,array:JSON.stringify(array)}, function(data){
		$('#review_list').html("");
		$('#review_list').html(data);
	});
	});
  
  $('.share').click(function(e){
	  e.preventDefault();
	  var id = $(this).attr("data-id");
  });
  
  $('#submit').click(function(e){
	  e.preventDefault();
	  var title = $('#title').val();
	  var review = $('#review').val();
	  var website_id = $('#website_id').val();
	  var rating = $('#rating').val();
	  var name = '<?php echo $_SESSION['name']; ?>';
	  var gravatar = '<?php echo $_SESSION['gravatar']; ?>';
	  
	  var btn2 = "";
	  var btn3 = "";
	  var btn4 = "";
	  var btn5 = "";
	  
	  if(rating > 1){ btn2 = "btn-warning"; }else{ btn2 = "btn-default btn-grey"; }
	  if(rating > 2){ btn3 = "btn-warning"; }else{ btn3 = "btn-default btn-grey"; }
	  if(rating > 3){ btn4 = "btn-warning"; }else{ btn4 = "btn-default btn-grey"; }
	  if(rating > 4){ btn5 = "btn-warning"; }else{ btn5 = "btn-default btn-grey"; }
	  
	  if (title == "" || review == ""){
		  alert("Please fill in all information and try again");
	  }else{
		  $.post("https://www.sudoseo.com/php/review.php", {title:title, review:review, website_id:website_id, rating:rating}, function(data){
			  $('#modal').modal("hide");
			  $('#review_list').prepend(
				'<div class="website_top_box container mt-sm-3">'+
					'<div class="row">'+
						'<div class="col-12">'+
							'<div style="display:flex;align-items:center">'+
							'<div style="float:left">'+
								'<img src="'+gravatar+'" class="rounded-circle" alt="Cinque Terre" style="border:1px solid #e2e2e2;max-width:45px;margin-right:10px">'+
							'</div>'+
							'<div style="float:left">'+
								'<div>'+name+'</div>'+
							'</div>'+
							'</div>'+
							'<div class="clearfix"></div>'+
							'<hr>'+
							'<div class="row">'+
								'<div class="col-6">'+
									'<div style="margin-bottom:5px;margin-top:5px" class="form-group">'+
										'<button style="margin-right:4px" type="button" class="btn btn-warning btn-sm" aria-label="Left Align">'+
										  '<i class="fa fa-star"></i>'+
										'</button>'+
										'<button style="margin-right:4px" type="button" class="btn btn-sm '+btn2+'" aria-label="Left Align">'+
										  '<i class="fa fa-star"></i>'+
										'</button>'+
										'<button style="margin-right:4px" type="button" class="btn btn-sm '+btn3+'" aria-label="Left Align">'+
										  '<i class="fa fa-star"></i>'+
										'</button>'+
										'<button style="margin-right:4px" type="button" class="btn btn-sm '+btn4+'" aria-label="Left Align">'+
										  '<i class="fa fa-star"></i>'+
										'</button>'+
										'<button style="margin-right:4px" type="button" class="btn btn-sm '+btn5+'" aria-label="Left Align">'+
										  '<i class="fa fa-star"></i>'+
										'</button>'+
									'</div>'+
								'</div>'+
								'<div class="col-6 text-right">'+
									'<span style="color:#777">'+data+'</span>'+
								'</div>'+
							'</div>'+
							'<br>'+
							'<div class="row">'+
								'<div class="col-12">'+
									'<div style="font-size:16px;font-weight:500">'+title+'</div>'+
									'<div class="mt-sm-2" style="font-size:14px">'+review+'</div>'+								
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'
			  );
			  $('#title').val("");
			  $('#review').val("");
		  });
	  }
  });
  
  $( ".rateButton" ).click(function(){
		if($(this).hasClass('btn-grey')) {			
			$(this).removeClass('btn-grey btn-default').addClass('btn-warning star-selected');
			$(this).prevAll('.rateButton').removeClass('btn-grey btn-default').addClass('btn-warning star-selected');
			$(this).nextAll('.rateButton').removeClass('btn-warning star-selected').addClass('btn-grey btn-default');			
		} else {						
			$(this).nextAll('.rateButton').removeClass('btn-warning star-selected').addClass('btn-grey btn-default');
		}
		$("#rating").val($('.star-selected').length);		
	});

  $(".progress_a").each(function() {
    var value = $(this).attr('data-value');
    var left = $(this).find('.progress-left .progress-bar');
    var right = $(this).find('.progress-right .progress-bar');

    if (value > 0) {
      if (value <= 50) {
        right.css('transform', 'rotate(' + percentageToDegrees(value) + 'deg)')
		right.css("border-color","#F0AD4E");
      } else {
        right.css('transform', 'rotate(180deg)')
        left.css('transform', 'rotate(' + percentageToDegrees(value - 50) + 'deg)')
		right.css("border-color","#0ACC00");
		left.css("border-color","#0ACC00");
      }
	  $(this).find(".pagespeedd").text(value);
    }

  });
  
  $('#update').click(function(e){
	  var id = '<?php echo $website_id; ?>';
	  var search = '<?php echo $website_url; ?>';
	  $.post("https://www.sudoseo.com/php/clean_url.php", {search:search}, function(s){
			$('#modal_site').text(s);
			$('#modal1').modal("show");
	  });
	  $.post("https://www.sudoseo.com/php/update_site.php", {id:id}, function(d){
		if (d == "Can't fetch website"){
			setTimeout(function(){
				$('#wait').html("<span style='color:red'>Can't fetch website data, try again later</span>");
				setTimeout(function(){
					$('#modal1').modal("hide");
				}, 2000);
			}, 3000);
		}else if (d == "Invalid domain"){
			setTimeout(function(){
				$('#wait').html("<span style='color:red'>Invalid domain, please try again</span>");
				setTimeout(function(){
					$("#modal1").modal("hide");
				}, 2000);
			}, 3000);
		}else{
			location.reload();
		}
	});
  });

  function percentageToDegrees(percentage) {

    return percentage / 100 * 360

  }

});

$(document).on("click", ".like", function(e){
  e.preventDefault();
  var id = $(this).attr("data-id");
  <?php if (isset($_SESSION['login'])) { ?>
  $.post("https://www.sudoseo.com/php/like.php", {id:id}, function(data){
	  if (data == "like"){
		  $("#like_"+id).html("<span style='color:blue'><i class='fa fa-thumbs-up'></i> Liked</span>");
	  }else{
		  $("#like_"+id).html("<i class='fa fa-thumbs-up'></i> Like");
	  }
  });
  <?php }else{ ?>
  $('#login').modal("show");
  <?php } ?>
});
</script>
</body>
</html>