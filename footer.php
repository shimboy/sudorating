<style>#signup_error,#login_error{display:none}</style>
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://www.sudoseo.com/js/autocomplete.js"></script>
<div style="background:#2a384f;color:#fff;padding-top:20px" class="text-center">
<span class="text-smaller text-gray-lighter mr-sm-2">
    © 2019 SUDOSEO. All Rights Reserved.
</span>

<ul style="margin-top:10px" class="list-inline list-unstyled display-inline-block text-smaller ml-md-2">
    <li class="list-inline-item mr-md-3">
        <a href="terms.php" class="text-gray-darker">
            Terms of Use
        </a>
    </li>
    <li class="list-inline-item mr-md-3">
        <a href="privacy.php" class="text-gray-darker">
            Privacy
        </a>
    </li>
        <li class="list-inline-item mr-md-3">
            <a href="cookies.php" class="text-gray-darker">
                Cookies
            </a>
        </li>
</ul>
</div>
<div id="signin" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Sign up to read and write reviews</h3>
            </div>
            <div class="modal-body">
				<div style="text-align:center">
					<a href="<?php echo $login; ?>"><img src="https://developers.google.com/identity/images/btn_google_signin_dark_normal_web.png"></a>
				</div><br>
				<div id="signup_error" class="alert alert-danger" role="alert"></div>
                <form class="form" role="form" autocomplete="off" id="formLogin" novalidate="" method="POST">
                    <div class="form-group">
                        <a href="" class="float-right">New user?</a>
                        <label for="uname1">Name</label>
                        <input type="text" class="form-control " name="uname1" id="name">
                    </div>
					 <div class="form-group">
                        <label for="uname1">Email Address</label>
                        <input type="text" class="form-control " name="uname1" id="email">
                    </div>
					<div class="form-group">
					<label for="country">Country</label>
					<select id="country" name="country" class="form-control ">
                <option value="Afghanistan">Afghanistan</option>
                <option value="Åland Islands">Åland Islands</option>
                <option value="Albania">Albania</option>
                <option value="Algeria">Algeria</option>
                <option value="American Samoa">American Samoa</option>
                <option value="Andorra">Andorra</option>
                <option value="Angola">Angola</option>
                <option value="Anguilla">Anguilla</option>
                <option value="Antarctica">Antarctica</option>
                <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                <option value="Argentina">Argentina</option>
                <option value="Armenia">Armenia</option>
                <option value="Aruba">Aruba</option>
                <option value="Australia">Australia</option>
                <option value="Austria">Austria</option>
                <option value="Azerbaijan">Azerbaijan</option>
                <option value="Bahamas">Bahamas</option>
                <option value="Bahrain">Bahrain</option>
                <option value="Bangladesh">Bangladesh</option>
                <option value="Barbados">Barbados</option>
                <option value="Belarus">Belarus</option>
                <option value="Belgium">Belgium</option>
                <option value="Belize">Belize</option>
                <option value="Benin">Benin</option>
                <option value="Bermuda">Bermuda</option>
                <option value="Bhutan">Bhutan</option>
                <option value="Bolivia">Bolivia</option>
                <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                <option value="Botswana">Botswana</option>
                <option value="Bouvet Island">Bouvet Island</option>
                <option value="Brazil">Brazil</option>
                <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                <option value="Brunei Darussalam">Brunei Darussalam</option>
                <option value="Bulgaria">Bulgaria</option>
                <option value="Burkina Faso">Burkina Faso</option>
                <option value="Burundi">Burundi</option>
                <option value="Cambodia">Cambodia</option>
                <option value="Cameroon">Cameroon</option>
                <option value="Canada">Canada</option>
                <option value="Cape Verde">Cape Verde</option>
                <option value="Cayman Islands">Cayman Islands</option>
                <option value="Central African Republic">Central African Republic</option>
                <option value="Chad">Chad</option>
                <option value="Chile">Chile</option>
                <option value="China">China</option>
                <option value="Christmas Island">Christmas Island</option>
                <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                <option value="Colombia">Colombia</option>
                <option value="Comoros">Comoros</option>
                <option value="Congo">Congo</option>
                <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
                <option value="Cook Islands">Cook Islands</option>
                <option value="Costa Rica">Costa Rica</option>
                <option value="Cote D'ivoire">Cote D'ivoire</option>
                <option value="Croatia">Croatia</option>
                <option value="Cuba">Cuba</option>
                <option value="Cyprus">Cyprus</option>
                <option value="Czech Republic">Czech Republic</option>
                <option value="Denmark">Denmark</option>
                <option value="Djibouti">Djibouti</option>
                <option value="Dominica">Dominica</option>
                <option value="Dominican Republic">Dominican Republic</option>
                <option value="Ecuador">Ecuador</option>
                <option value="Egypt">Egypt</option>
                <option value="El Salvador">El Salvador</option>
                <option value="Equatorial Guinea">Equatorial Guinea</option>
                <option value="Eritrea">Eritrea</option>
                <option value="Estonia">Estonia</option>
                <option value="Ethiopia">Ethiopia</option>
                <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                <option value="Faroe Islands">Faroe Islands</option>
                <option value="Fiji">Fiji</option>
                <option value="Finland">Finland</option>
                <option value="France">France</option>
                <option value="French Guiana">French Guiana</option>
                <option value="French Polynesia">French Polynesia</option>
                <option value="French Southern Territories">French Southern Territories</option>
                <option value="Gabon">Gabon</option>
                <option value="Gambia">Gambia</option>
                <option value="Georgia">Georgia</option>
                <option value="Germany">Germany</option>
                <option value="Ghana">Ghana</option>
                <option value="Gibraltar">Gibraltar</option>
                <option value="Greece">Greece</option>
                <option value="Greenland">Greenland</option>
                <option value="Grenada">Grenada</option>
                <option value="Guadeloupe">Guadeloupe</option>
                <option value="Guam">Guam</option>
                <option value="Guatemala">Guatemala</option>
                <option value="Guernsey">Guernsey</option>
                <option value="Guinea">Guinea</option>
                <option value="Guinea-bissau">Guinea-bissau</option>
                <option value="Guyana">Guyana</option>
                <option value="Haiti">Haiti</option>
                <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
                <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                <option value="Honduras">Honduras</option>
                <option value="Hong Kong">Hong Kong</option>
                <option value="Hungary">Hungary</option>
                <option value="Iceland">Iceland</option>
                <option value="India">India</option>
                <option value="Indonesia">Indonesia</option>
                <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                <option value="Iraq">Iraq</option>
                <option value="Ireland">Ireland</option>
                <option value="Isle of Man">Isle of Man</option>
                <option value="Israel">Israel</option>
                <option value="Italy">Italy</option>
                <option value="Jamaica">Jamaica</option>
                <option value="Japan">Japan</option>
                <option value="Jersey">Jersey</option>
                <option value="Jordan">Jordan</option>
                <option value="Kazakhstan">Kazakhstan</option>
                <option value="Kenya">Kenya</option>
                <option value="Kiribati">Kiribati</option>
                <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
                <option value="Korea, Republic of">Korea, Republic of</option>
                <option value="Kuwait">Kuwait</option>
                <option value="Kyrgyzstan">Kyrgyzstan</option>
                <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                <option value="Latvia">Latvia</option>
                <option value="Lebanon">Lebanon</option>
                <option value="Lesotho">Lesotho</option>
                <option value="Liberia">Liberia</option>
                <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                <option value="Liechtenstein">Liechtenstein</option>
                <option value="Lithuania">Lithuania</option>
                <option value="Luxembourg">Luxembourg</option>
                <option value="Macao">Macao</option>
                <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                <option value="Madagascar">Madagascar</option>
                <option value="Malawi">Malawi</option>
                <option value="Malaysia">Malaysia</option>
                <option value="Maldives">Maldives</option>
                <option value="Mali">Mali</option>
                <option value="Malta">Malta</option>
                <option value="Marshall Islands">Marshall Islands</option>
                <option value="Martinique">Martinique</option>
                <option value="Mauritania">Mauritania</option>
                <option value="Mauritius">Mauritius</option>
                <option value="Mayotte">Mayotte</option>
                <option value="Mexico">Mexico</option>
                <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                <option value="Moldova, Republic of">Moldova, Republic of</option>
                <option value="Monaco">Monaco</option>
                <option value="Mongolia">Mongolia</option>
                <option value="Montenegro">Montenegro</option>
                <option value="Montserrat">Montserrat</option>
                <option value="Morocco">Morocco</option>
                <option value="Mozambique">Mozambique</option>
                <option value="Myanmar">Myanmar</option>
                <option value="Namibia">Namibia</option>
                <option value="Nauru">Nauru</option>
                <option value="Nepal">Nepal</option>
                <option value="Netherlands">Netherlands</option>
                <option value="Netherlands Antilles">Netherlands Antilles</option>
                <option value="New Caledonia">New Caledonia</option>
                <option value="New Zealand">New Zealand</option>
                <option value="Nicaragua">Nicaragua</option>
                <option value="Niger">Niger</option>
                <option value="Nigeria">Nigeria</option>
                <option value="Niue">Niue</option>
                <option value="Norfolk Island">Norfolk Island</option>
                <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                <option value="Norway">Norway</option>
                <option value="Oman">Oman</option>
                <option value="Pakistan">Pakistan</option>
                <option value="Palau">Palau</option>
                <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                <option value="Panama">Panama</option>
                <option value="Papua New Guinea">Papua New Guinea</option>
                <option value="Paraguay">Paraguay</option>
                <option value="Peru">Peru</option>
                <option value="Philippines">Philippines</option>
                <option value="Pitcairn">Pitcairn</option>
                <option value="Poland">Poland</option>
                <option value="Portugal">Portugal</option>
                <option value="Puerto Rico">Puerto Rico</option>
                <option value="Qatar">Qatar</option>
                <option value="Reunion">Reunion</option>
                <option value="Romania">Romania</option>
                <option value="Russian Federation">Russian Federation</option>
                <option value="Rwanda">Rwanda</option>
                <option value="Saint Helena">Saint Helena</option>
                <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                <option value="Saint Lucia">Saint Lucia</option>
                <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
                <option value="Samoa">Samoa</option>
                <option value="San Marino">San Marino</option>
                <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                <option value="Saudi Arabia">Saudi Arabia</option>
                <option value="Senegal">Senegal</option>
                <option value="Serbia">Serbia</option>
                <option value="Seychelles">Seychelles</option>
                <option value="Sierra Leone">Sierra Leone</option>
                <option value="Singapore">Singapore</option>
                <option value="Slovakia">Slovakia</option>
                <option value="Slovenia">Slovenia</option>
                <option value="Solomon Islands">Solomon Islands</option>
                <option value="Somalia">Somalia</option>
                <option value="South Africa">South Africa</option>
                <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
                <option value="Spain">Spain</option>
                <option value="Sri Lanka">Sri Lanka</option>
                <option value="Sudan">Sudan</option>
                <option value="Suriname">Suriname</option>
                <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                <option value="Swaziland">Swaziland</option>
                <option value="Sweden">Sweden</option>
                <option value="Switzerland">Switzerland</option>
                <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                <option value="Taiwan, Province of China">Taiwan, Province of China</option>
                <option value="Tajikistan">Tajikistan</option>
                <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                <option value="Thailand">Thailand</option>
                <option value="Timor-leste">Timor-leste</option>
                <option value="Togo">Togo</option>
                <option value="Tokelau">Tokelau</option>
                <option value="Tonga">Tonga</option>
                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                <option value="Tunisia">Tunisia</option>
                <option value="Turkey">Turkey</option>
                <option value="Turkmenistan">Turkmenistan</option>
                <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                <option value="Tuvalu">Tuvalu</option>
                <option value="Uganda">Uganda</option>
                <option value="Ukraine">Ukraine</option>
                <option value="United Arab Emirates">United Arab Emirates</option>
                <option value="United Kingdom">United Kingdom</option>
                <option value="United States">United States</option>
                <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                <option value="Uruguay">Uruguay</option>
                <option value="Uzbekistan">Uzbekistan</option>
                <option value="Vanuatu">Vanuatu</option>
                <option value="Venezuela">Venezuela</option>
                <option value="Viet Nam">Viet Nam</option>
                <option value="Virgin Islands, British">Virgin Islands, British</option>
                <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                <option value="Wallis and Futuna">Wallis and Futuna</option>
                <option value="Western Sahara">Western Sahara</option>
                <option value="Yemen">Yemen</option>
                <option value="Zambia">Zambia</option>
                <option value="Zimbabwe">Zimbabwe</option>
            </select>
			</div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control " id="password">
                    </div>
                    <div class="">
                      <input type="checkbox" id="accept"> I accept the <a style="color:#79a9d9" href="terms.php" target="_blank">Terms & Conditions</a> and <a style="color:#79a9d9" href="privacy.php" target="_blank">Privacy Policy</a>
                    </div>
                    <div class="form-group py-4">
                        <button class="btn btn-outline-secondary btn-lg" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        <button type="submit" class="btn btn-success btn-lg float-right" id="signup_button">Sign up</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="login" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Login to read and write reviews</h3>
            </div>
            <div class="modal-body">
				<div style="text-align:center">
					<a href="<?php echo $login; ?>"><img src="https://developers.google.com/identity/images/btn_google_signin_dark_normal_web.png"></a>
				</div><br>
				<div id="login_error" class="alert alert-danger" role="alert"></div>
                <form class="form" role="form" autocomplete="off" id="formLogin" novalidate="" method="POST">
                    <div class="form-group">
                        <a href="" class="float-right">New user?</a>
                        <label for="uname1">Email Address</label>
                        <input type="text" class="form-control " id="login_email">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control " id="login_password">
                    </div>
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" id="rememberMe">
                      <label class="custom-control-label" for="rememberMe">Remember me on this computer</label>
                    </div>
                    <div class="form-group py-4">
                        <button class="btn btn-outline-secondary btn-lg" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        <button type="submit" class="btn btn-success btn-lg float-right" id="login_button">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="crawl_modal" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div style="background:#2a384f;color:#fff" class="modal-content">
      <div class="modal-body">
        <div class="text-center">
			<br>
			<div style="font-size:18px;font-weight:500;color:#e2e2e2" class="modal_site"></div><br><br>
			<div class="wait" style="color:#e2e2e2">Fetching data, please wait...</div><br>
			 <div style="height:20px;margin-bottom:25px" class="progresss">
			  <div class="indeterminate"></div>
		  </div>
			<div style="font-size:12px;color:#e2e2e2">This may take a few moments while we gather all website information</div>
			<br><br>
		</div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
	var options = {
		url: function(phrase) {
			return "https://www.sudoseo.com/php/sites.php?phrase=" + phrase;
		},
		
		getValue: "name",
		
		template: {
			type: "custom",
			method: function(value, item) {
				return "<a style='color:#333' href='https://www.sudoseo.com/website/"+item.type+"'><div style='float:left'><img style='border:1px solid #e2e2e2;max-height:40px' src='" + item.icon + "' /></div> <div style='float:left'>"+value+"<br><span style='font-size:12px;color:#777'>"+item.link+"<span></div></a><div style='clear:both'></div>";
			}
		}
	};

	$("#search").easyAutocomplete(options);
	
	$('#form').submit(function(e){
		e.preventDefault();
		$('.wait').text("Fetching data, please wait...");
		var search = $('#search').val();
		if (search != ""){
			$.get("https://www.sudoseo.com/php/website_exist.php", {search:search}, function(data){
				if (data == "false"){
					$.post("https://www.sudoseo.com/php/clean_url.php", {search:search}, function(s){
						$('.modal_site').text(s);
						$('#crawl_modal').modal("show");
					});
					$.post("https://www.sudoseo.com/php/add_site.php", {search:search}, function(d){
						if (d == "Can't fetch website"){
							setTimeout(function(){
								$('.wait').html("<span style='color:red'>Can't fetch website data, try again later</span>");
								setTimeout(function(){
									$('#crawl_modal').modal("hide");
								}, 2000);
							}, 3000);
						}else if (d == "Invalid domain"){
							setTimeout(function(){
								$('.wait').html("<span style='color:red'>Invalid domain, please try again</span>");
								setTimeout(function(){
									$("#crawl_modal").modal("hide");
								}, 2000);
							}, 3000);
						}else{
							window.location.href = "website/"+d;
						}
					});
				}else{
					window.location.href = "website/"+data;
				}
			});
		}else{
			alert("Please enter a website and try again");
		}
	});
	
	$(".search").focus(function(){
		$(this).css("background", "#fff");
		$(this).css("color", "#333");
	});
	$('.search').blur( function() {
		$(this).css("background", "#2a384f");
		$(this).css("color", "#fff");
	});
	$('#signup_button').click(function(e){
		e.preventDefault();
		var name = $('#name').val();
		var email = $('#email').val();
		var password = $('#password').val();
		var country = $('#country').val();
		var has_error = false;
		
		if ($('#accept').not(':checked').length) {
			has_error = true;
			$('#signup_error').text("Please accept the terms to sign up");
			$('#signup_error').slideDown();
		}
		
		if (name == "" || email == "" || password == ""){
			has_error = true;
			$('#signup_error').text("Please fill out all information");
			$('#signup_error').slideDown();
		}
		
		if (!isEmail(email)){
			has_error = true;
			$('#signup_error').text("Please enter a valid email address");
			$('#signup_error').slideDown();
		}
		
		if (has_error == false){
			$.post("https://www.sudoseo.com/php/signup.php", {name:name, email:email, password:password, country:country}, function(data){
				if (data == "exists"){
					$('#signup_error').text("That email address is already in use, please try logging in");
					$('#signup_error').slideDown();
				}else if (data == "success"){
					window.location.href = "account";
				}
			});
		}
	});
	
	$('#login_button').click(function(e){
		e.preventDefault();
		var email = $('#login_email').val();
		var password = $('#login_password').val();
		var has_error = false;
		
		if (email == "" || password == ""){
			has_error = true;
			$('#login_error').text("Please fill out all information");
			$('#login_error').slideDown();
		}
		
		if (has_error == false){
			$.post("https://www.sudoseo.com/php/login.php", {email:email, password:password}, function(data){
				if (data == "error"){
					$('#login_error').text("Invalid login credentials, please try again");
					$('#login_error').slideDown();
				}else if (data == "success"){
					window.location.href = "account";
				}
			});
		}
	});
});

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
</script>