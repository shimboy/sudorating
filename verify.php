<?php

if (!isset($_GET['code'])){ header("Location: index.php"); }

include "header.php";
require_once("php/functions.php");
require_once("php/user_functions.php");

$error = 0;

if (isset($_POST['submit'])){
	$code = $_POST['code'];
	$sql = $con->prepare("SELECT * FROM users WHERE verification_code = :code");
	$sql->bindParam(":code", $code);
	$sql->execute();
	if ($sql->rowCount() > 0){
		$row = $sql->fetch(PDO::FETCH_ASSOC);
		$id = $row['id'];
		$sql = $con->prepare("UPDATE users SET verified = 1, verification_code = '' WHERE id = :id");
		$sql->bindParam(":id", $id);
		$sql->execute();
		$error = 2;
	}else{
		$error = 1;
	}
}

?>
<style>
.pagination a {
  color: black;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
  transition: background-color .3s;
  border: 1px solid #ddd;
}

.pagination a.active {
  background-color: #4CAF50;
  color: white;
  border: 1px solid #4CAF50;
}
</style>
<div class="homepage_background_image">
	<div style="padding:0" class="main container-fluid">
	<div class="container"><br><br>
		<div style="width:50%;margin:0 auto">
		<?php if ($error == 1){ ?>
		<div class="alert alert-danger" role="alert">Incorrect verification code, plese try again</div>
		<?php } ?>
		<?php if ($error == 2){ ?>
		<div class="alert alert-success" role="alert">Thank you, your account is now verified</div>
		<?php } ?>
		<?php if ($error != 2){ ?>
		<h2>Verify Account</h2><br>
		<form action="" method="POST">
		<label for="code">Enter verification code</label>
		<input id="code" type="text" name="code" class="form-control" value="<?php echo $_GET['code']; ?>">
		<button style="margin-top:10px" type="submit" name="submit" class="btn btn-primary">Verify account</button>
		</form>
		<?php } ?>
		</div>
		<br><br><br><br>
		</div>
		</div>
	</div>
	</div>
</div>

<?php include "footer.php"; ?>
<script>

</script>

</body>
</html>