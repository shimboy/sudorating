<?php

if (!isset($_SESSION)){ session_start(); }
if (!isset($_SESSION['login'])){ header("Location: index.php"); }

require_once("php/functions.php");
require_once("php/user_functions.php");
include "header.php";

$data = getProfileData($_SESSION['id']);
$my_sites = getMySites($_SESSION['id']);

?>

<div class="wrapper">
<div class="rp_top_bar">
	<div class="rp_top_bar_container container">
<div class="container emp-profile">
            <form method="post">
                <div class="row" style="display:flex;align-items:center">
                    <div class="col-md-2">
                        <div class="profile-img">
                            <img style="border-radius:50%" src="<?php echo $data['gravatar']; ?>">
							<div style="height:15px">&nbsp;</div>
						</div>
                    </div>
                    <div class="col-md-6">
                        <div class="profile-head">
                                    <h5>
                                        <?php echo ucwords($data['name']); ?>
                                    </h5>
                                    <h6>
                                        <?php echo $data['country']; ?>
                                    </h6>
									<div style="height:15px">&nbsp;</div>
                        </div>
                    </div>
					<div class="col-md-4">
					<div style="text-align:center">
						<div style="float:left;margin-right:20%">
						<p class="proile-rating"><?php getReviewCount($data['id']); ?> <div style="margin-top:10px">Reviews</div></p>
						</div>
						<div style="float:left;margin-right:20%">
						<p class="proile-rating"><?php getReadCount($data['id']); ?> <div style="margin-top:10px">Reads</div></p>
						</div>
						<div style="float:left">
						<p class="proile-rating"><?php getLikeCount($data['id']); ?> <div style="margin-top:10px">Likes</div></p>
						</div>
						</div>
						<div class="clearfix"></div>
					</div>
                </div>
			</form>
</div>
</div>
</div>

<?php if ($data['verified'] == 0){ ?>
<br>
<div class="container">
<div class="alert alert-danger" role="alert">
<div style="margin-top:10px"><strong>Verify email address</strong></div>
<p>Please verify your email by clicking the link within the email we sent you.</p>
</div>
</div>
<?php }else{ ?>
<br>
<?php } ?>
<div class="homepage_background_image">
	<div style="background:none" class="main container-fluid">
		<div class="container">
		<div class="row">
				<div id="results" class="col-md-8">
				<div class="website_top_box mt-sm-3">
				<h2>My Websites</h2><br>
		<?php
		$sql = $con->prepare("SELECT * FROM websites WHERE owner = :id");
		$sql->bindParam(":id", $_SESSION['id']);
		$sql->execute();
		while($website = $sql->fetch(PDO::FETCH_ASSOC)){
			$ratings = $con->prepare("SELECT * FROM reviews WHERE website_id = :id ORDER BY id DESC");
			$ratings->bindParam(":id", $website['id']);
			$ratings->execute();

			$r = array();
			while($rr = $ratings->fetch(PDO::FETCH_ASSOC)){
				$r[] = $rr['rating'];
			}

			$average = 0;
			if(count($r)) {
				$a = array_filter($r);
				$average = array_sum($a)/count($a);
			}

			$average = number_format($average, 1);
		?>
		<div class="website row">
			<div class="col-12">
				<div style="width:100%;background:#f0f0f0;padding:10px;margin-bottom:10px">
					<div class="row">
						<div class="col-md-6">
							<div style="float:left">
								<a href="website/<?php echo $website['name']; ?>"><img src="<?php echo $website['screenshot']; ?>" style="border:1px solid #e2e2e2;max-height:60px"></a>
							</div>
							<div style="float:left;margin-left:10px">
								<h3><a style="color:#333" href="website/<?php echo $website['name']; ?>"><?php 
								$name = ucwords($website['name']);
								echo substr($name, 0, strpos($name, "."));
								?></a></h3>
								<div style="font-size:0.9em;color:#777">
								<?php
								$url = str_replace(array("http://", "https://", "www."), "", $website['url']);
								if (strpos($url, '/') !== false){
								echo substr($url, 0, strpos($url, "/"));
								}else{
								echo $url;
								}
								?>
								</div>
								<div style="font-size:0.9em;color:#555"><?php echo $ratings->rowCount(); ?> reviews</div>
							</div>
						</div>
						<div class="col-md-6 justify-content-right" style="display:flex;align-items:center">
							<div style="margin-left:auto;margin-bottom:5px;margin-top:5px" class="form-group">
								<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
								  <i class="fa fa-star"></i>
								</button>
								<button type="button" class="btn btn-sm <?php if ($average > 1){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
								  <i class="fa fa-star"></i>
								</button>
								<button type="button" class="btn btn-sm <?php if ($average > 2){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
								  <i class="fa fa-star"></i>
								</button>
								<button type="button" class="btn btn-sm <?php if ($average > 3){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
								  <i class="fa fa-star"></i>
								</button>
								<button type="button" class="btn btn-sm <?php if ($average > 4){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
								  <i class="fa fa-star"></i>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
			</div>
			</div>
			<div class="col-md-4">
			<div class="website_top_box container mt-sm-3">
				<h3>Top Rated Sites</h3>
				<hr>
				<div style="width:100%;background:#f0f0f0;padding:10px;margin-bottom:10px">
					<?php 
					$count = 0;
					foreach(top_rated_sites() as $key => $val){
						$count++;
						if ($count > 10) break;
						$website = getWebsiteData($key);
					?>
					<div style="width:99%;margin:-left aut;margin-right:auto;margin-bottom:10px;background:#fff;padding:10px">
					<div class="row">
						<div class="col-12">
								<div class="row">
									<div class="col-md-10">
										<div style="float:left">
											<a style="color:#333" href="website/<?php echo $website['name']; ?>"><img src="<?php echo $website['screenshot']; ?>" style="margin-right:2px;border:1px solid #e2e2e2;max-height:30px"></a>
										</div>
										<div style="float:left;margin-left:10px">
											<h3 style="font-size:1em"><a style="color:#333" href="website/<?php echo $website['name']; ?>"><?php 
											$name = ucwords($website['name']);
											echo substr($name, 0, strpos($name, "."));
											?></a></h3>
										</div>
									</div>
									<div class="col-md-2">
										<div style="font-weight:500px;color:#2a384f"><?php echo $val; ?></div>
									</div>
								</div>
						</div>
					</div>
					</div>
					<?php } ?>
					</div>
			</div>
			
			<div class="website_top_box container mt-sm-3">
				<div class="row">
					<div class="col-md-12">
						<div style="width:120px;height:600px;border:1px solid #e2e2e2;margin:0 auto">
							<img src="img/skyscraper.png">
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
<br><br>
</div>
</div>

<?php include "footer.php"; ?>

<script>

</script>
</body>
</html>