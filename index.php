<?php

include "header.php";
require_once("php/functions.php");
require_once("php/user_functions.php");

?>
<style>
.pagination a {
  color: black;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
  transition: background-color .3s;
  border: 1px solid #ddd;
}

.pagination a.active {
  background-color: #4CAF50;
  color: white;
  border: 1px solid #4CAF50;
}
</style>
<div class="homepage_background_image">
	<div style="padding:0" class="main container-fluid">
	<div id="blue" style="background:#6ea6e6">
	<div class="htext"><br><br><br><br></div>
	<div class="text-center">
		<h1 class="homepage_h1" style="margin-bottom:0;color:#fff">Behind every review is an experience that matters</h1>
		<h3 class="homepage_h3 mt-sm-1" style="color:#fff">Read reviews. Write reviews. Find companies. Submit websites.</h3> 
		<h3 class="homepage_h3_mobile" style="color:#fff"><br><br>Search for a website below</h3>
	</div>
	<br><br>
	<div class="mt-sm-5">
		<form id="form" style="border:1px solid #e2e2e2" class="hero__search-form search-form" role="search" action="" method="get" autocomplete="false" data-hero-search-form="">
            <span class="hero__search-form__placeholder-icon icon-search"></span>
            <input id="search" class="hero__search-input search-input" type="search" name="query" data-home-search-input="" placeholder="Search for a website" aria-label="Search for a company…" autofocus="" autocomplete="off">
            <button class="hero__search-form__submit" type="submit" aria-label="Search">
                <span class="hero__search-form__submit__text">Search</span>
                <span class="hero__search-form__submit__icon icon-search"></span>
            </button>
			<div class="autocomplete-suggestions" style="position: absolute; display: none; z-index: 9999;"></div>
		</form>
	</div>
	<div class="htext"><br><br><br><br></div><br><br>
	</div>
	<br>
	<div class="container mt-sm-4">
		<hr>
		<br>
		<div style="width:90%;margin:0 auto" class="row">
			<div id="results" class="col-md-8">
		<?php
		$sql = $con->prepare("SELECT * FROM websites");
		$sql->execute();
		while($website = $sql->fetch(PDO::FETCH_ASSOC)){
			$ratings = $con->prepare("SELECT * FROM reviews WHERE website_id = :id ORDER BY id DESC");
			$ratings->bindParam(":id", $website['id']);
			$ratings->execute();

			$r = array();
			while($rr = $ratings->fetch(PDO::FETCH_ASSOC)){
				$r[] = $rr['rating'];
			}

			$average = 0;
			if(count($r)) {
				$a = array_filter($r);
				$average = array_sum($a)/count($a);
			}

			$average = number_format($average, 1);
		?>
		<div class="website row">
			<div class="col-12">
				<div style="width:100%;background:#f0f0f0;padding:10px;margin-bottom:10px">
					<div class="row">
						<div class="col-md-6">
							<div style="float:left">
								<a href="website/<?php echo $website['name']; ?>"><img src="<?php echo $website['screenshot']; ?>" style="border:1px solid #e2e2e2;max-height:60px"></a>
							</div>
							<div style="float:left;margin-left:10px">
								<h3><a style="color:#333" href="website/<?php echo $website['name']; ?>"><?php 
								$name = ucwords($website['name']);
								echo substr($name, 0, strpos($name, "."));
								?></a></h3>
								<div style="font-size:0.9em;color:#777">
								<?php
								$url = str_replace(array("http://", "https://", "www."), "", $website['url']);
								if (strpos($url, '/') !== false){
								echo substr($url, 0, strpos($url, "/"));
								}else{
								echo $url;
								}
								?>
								</div>
								<div style="font-size:0.9em;color:#555"><?php echo $ratings->rowCount(); ?> reviews</div>
							</div>
						</div>
						<div class="col-md-6 justify-content-right" style="display:flex;align-items:center">
							<div style="margin-left:auto;margin-bottom:5px;margin-top:5px" class="form-group">
								<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
								  <i class="fa fa-star"></i>
								</button>
								<button type="button" class="btn btn-sm <?php if ($average > 1){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
								  <i class="fa fa-star"></i>
								</button>
								<button type="button" class="btn btn-sm <?php if ($average > 2){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
								  <i class="fa fa-star"></i>
								</button>
								<button type="button" class="btn btn-sm <?php if ($average > 3){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
								  <i class="fa fa-star"></i>
								</button>
								<button type="button" class="btn btn-sm <?php if ($average > 4){ ?> btn-warning <?php }else{ ?> btn-default btn-grey <?php } ?>" aria-label="Left Align">
								  <i class="fa fa-star"></i>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<ul style="justify-content:center" class="pagination">
			<li class="page-item">
				<a id="previous-page" class="page-link" href="javascriot:void(0)" aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
				</a>
			</li>
		</ul>
		</div>
		<div class="col-12 col-md-4">
				<div style="width:100%;background:#f0f0f0;padding:10px;margin-bottom:10px">
					<h3 style="margin-top:5px" class="text-center">Best Rated Websites</h3>
					<hr>
					<?php
					$count = 0;
					foreach(top_rated_sites() as $key => $val){
						$count++;
						if ($count > 10) break;
						$website = getWebsiteData($key);
					?>
					<div style="width:99%;margin:-left aut;margin-right:auto;margin-bottom:10px;background:#fff;padding:10px">
					<div class="row">
						<div class="col-12">
								<div class="row">
									<div class="col-md-10">
										<div style="float:left">
											<a href="website/<?php echo $website['name']; ?>"><img src="<?php echo $website['screenshot']; ?>" style="margin-right:2px;border:1px solid #e2e2e2;max-height:30px"></a>
										</div>
										<div style="float:left;margin-left:10px">
											<h3 style="font-size:1em"><a style="color:#333" href="website/<?php echo $website['name']; ?>"><?php 
											$name = ucwords($website['name']);
											echo substr($name, 0, strpos($name, "."));
											?></a></h3>
										</div>
									</div>
									<div class="col-md-2">
										<div class="rating-value"><?php echo $val; ?></div>
									</div>
								</div>
						</div>
					</div>
					</div>
					<?php } ?>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
	<br><br>
	</div>
</div>

<?php include "footer.php"; ?>
<script>
$(function(){
	var results = $('#results .website').length;
	var limitPerPage = 10;
	$('#results .website:gt('+(limitPerPage -1)+')').hide();
	var totalPages = Math.round(results / limitPerPage);
	$('.pagination').append("<li class='page-item current-page active'><a class='page-link' href='javascript:void(0)'>"+1+"</a></li>");
	
	for(var i = 2; i <= totalPages; i++){
		$('.pagination').append("<li class='page-item current-page'><a class='page-link' href='javascript:void(0)'>"+i+"</a></li>");
	}
	
	$('.pagination').append("<li id='next-page' class='page-item'><a class='page-link' href='javascriot:void(0)' aria-label='Next'><span aria-hidden='true'>&raquo;</span></a></li>");
	
	$('.pagination li.current-page').click(function(){
		if ($(this).hasClass("active")){
			return false;
		}else{
			var currentPage = $(this).index();
			$(".pagination li").removeClass("active");
			$(this).addClass("active");
			$('#results .website').hide();
			
			var grandTotal = limitPerPage * currentPage;
			for(var i = grandTotal - limitPerPage; i < grandTotal; i++){
				$('#results .website:eq('+i+')').show();
			}
		}
	});
	
	$('#next-page').on("click", function(){
		var currentPage = $('.pagination li.active').index();
		if (currentPage == totalPages){
			return false;
		}else{
			currentPage++;
			$(".pagination li").removeClass("active");
			$('#results .website').hide();
			
			var grandTotal = limitPerPage * currentPage;
			for(var i = grandTotal - limitPerPage; i < grandTotal; i++){
				$('#results .website:eq('+i+')').show();
			}
			$('.pagination li.current-page:eq('+(currentPage - 1)+')').addClass("active");
		}
	});
	
	$('#previous-page').on("click", function(){
		var currentPage = $('.pagination li.active').index();
		if (currentPage == 1){
			return false;
		}else{
			currentPage--;
			$(".pagination li").removeClass("active");
			$('#results .website').hide();
			
			var grandTotal = limitPerPage * currentPage;
			for(var i = grandTotal - limitPerPage; i < grandTotal; i++){
				$('#results .website:eq('+i+')').show();
			}
			$('.pagination li.current-page:eq('+(currentPage - 1)+')').addClass("active");
		}
	});
});
</script>

</body>
</html>