<?php

session_start();
require_once("php/user_functions.php");
require_once("php/functions.php");
if (!isset($_SESSION['login'])){ header("Location: index.php"); }
if (!isset($_GET['website_id'])){ header("Location: index.php"); }
include "header.php";

$website = getWebsiteData($_GET['website_id']);
$data = getProfileData($_SESSION['id']);

$website_url = str_replace(array("https://", "http://", "www."), "", $website['url']);
$url = $website['url'];
$website_id = $website['id'];

?>
<style>#error{display:none}</style>
<div class="wrapper">
<div class="rp_top_bar">
	<div class="rp_top_bar_container container">
<div class="container emp-profile">
            <form method="post">
                <div class="row" style="display:flex;align-items:center">
                    <div class="col-md-2">
                        <div class="profile-img">
                            <img style="border-radius:50%" src="<?php echo $data['gravatar']; ?>">
							<div style="height:15px">&nbsp;</div>
						</div>
                    </div>
                    <div class="col-md-6">
                        <div class="profile-head">
                                    <h5>
                                        <?php echo ucwords($data['name']); ?>
                                    </h5>
                                    <h6>
                                        <?php echo $data['country']; ?>
                                    </h6>
									<div style="height:15px">&nbsp;</div>
                        </div>
                    </div>
					<div class="col-md-4">
					<div style="text-align:center">
						<div style="float:left;margin-right:20%">
						<p class="proile-rating"><?php getReviewCount($data['id']); ?> <div style="margin-top:10px">Reviews</div></p>
						</div>
						<div style="float:left;margin-right:20%">
						<p class="proile-rating"><?php getReadCount($data['id']); ?> <div style="margin-top:10px">Reads</div></p>
						</div>
						<div style="float:left">
						<p class="proile-rating"><?php getLikeCount($data['id']); ?> <div style="margin-top:10px">Likes</div></p>
						</div>
						</div>
						<div class="clearfix"></div>
					</div>
                </div>
			</form>
</div>
</div>
</div>

<br>
<div class="homepage_background_image">
	<div style="background:none" class="main container-fluid">
		<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="website_top_box container mt-sm-3">
					<div class="row">
						<div class="col-12">
						<div style="font-size:1.4em;font-weight:500">Choose a way to verify <span style="color:#a1a1a1"><?php echo $website_url; ?></span></div>
						<hr>
						<br>
						<div id="error" class="alert alert-danger" role="alert"></div>
						<ul class="rp_tabs nav nav-tabs">
							  <li class="nav-item">
								<a data-toggle="tab" href="#fileupload" role="tab" aria-controls="nav-home" aria-selected="true" class="nav-link active" href="#">File Upload</a>
							  </li>
							  <li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#metatag" role="tab" aria-controls="nav-home" aria-selected="false">Meta Tag</a>
							  </li>
						</ul>
						<div class="tab-content">
						<div style="font-size:1.1em" class="tab-pane fade show active" id="fileupload" role="tabpanel">
						<br>
							<div style="padding:10px">
								Verify that you own <strong><?php echo $website_url; ?></strong> by uploading an HTML file to your website
							</div>
							<br>
							<ul>
								<li style="padding:10px">Download this <a style="color:#79a9d9" href="php/download.php">HTML verification file</a></li>
								<li style="padding:10px">Upload the file to <strong><?php echo $website_url; ?></strong> root folder</li>
								<li style="padding:10px">Click the Verify Domain button below</li>
							</ul>
							<br>
							<button type="submit" id="verify_html" class="btn btn-primary">Verify Domain</button>
						</div>
						<div style="font-size:1.1em" class="tab-pane fade" id="metatag" role="tabpanel">
						<br>
							<div style="padding:10px">
								Verify that you own <strong><?php echo $website_url; ?></strong> by adding a meta tag to your home page
							</div>
							<br>
							<ul>
								<li style="padding:10px">Copy the meta tag below and paste it in the HTML code of your site’s home page. It should go in the <strong>head</strong> section.</li>
								<li style="padding:10px">
								<div style="width:100%;margin:0;padding:20px;font-size:0.9em;color:#333" class="jumbotron">
								<?php echo htmlspecialchars('<meta name="sudoseo-verification-id" content="'.md5("verification").'"/>'); ?>
								</div>
								</li>
								<li style="padding:10px">Click the Verify Domain button below</li>
							</ul>
							<br>
							<button type="submit" id="verify_meta" class="btn btn-primary">Verify Domain</button>
						</div>
						</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
			<div class="website_top_box container mt-sm-3">
				<h3>Featured Sites</h3>
				<hr>
				<div style="width:100%;background:#f0f0f0;padding:10px;margin-bottom:10px">
					<?php 
					foreach(getWebsites() as $website){
						$ratings = $con->prepare("SELECT * FROM reviews WHERE website_id = :id ORDER BY id DESC");
						$ratings->bindParam(":id", $website['id']);
						$ratings->execute();

						$r = array();
						while($rr = $ratings->fetch(PDO::FETCH_ASSOC)){
							$r[] = $rr['rating'];
						}

						$average = 0;
						if(count($r)) {
							$a = array_filter($r);
							$average = array_sum($a)/count($a);
						}

						$average = number_format($average, 1);
					?>
					<div style="width:99%;margin:-left aut;margin-right:auto;margin-bottom:10px;background:#fff;padding:10px">
					<div class="row">
						<div class="col-12">
								<div class="row">
									<div class="col-md-10">
										<div style="float:left">
											<img src="<?php echo $website['screenshot']; ?>" style="margin-right:2px;border:1px solid #e2e2e2;max-height:30px">
										</div>
										<div style="float:left;margin-left:10px">
											<h3 style="font-size:1em"><a style="color:#333" href="website.php?website_id=<?php echo $website['id']; ?>"><?php 
											$name = ucwords($website['name']);
											echo substr($name, 0, strpos($name, "."));
											?></a></h3>
										</div>
									</div>
									<div class="col-md-2">
										<div style="font-weight:500px;color:#2a384f"><?php echo $average; ?></div>
									</div>
								</div>
						</div>
					</div>
					</div>
					<?php } ?>
					</div>
			</div>
			
			<div class="website_top_box container mt-sm-3">
				<div class="row">
					<div class="col-md-12">
						<div style="width:120px;height:600px;border:1px solid #e2e2e2;margin:0 auto">
							<img src="img/skyscraper.png">
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
<br><br>
</div>
</div>

<?php include "footer.php"; ?>
<script>
$(function(){
	$('#verify_html').click(function(e){
		e.preventDefault();
		var website = '<?php echo $url; ?>';
		var id = '<?php echo $website_id; ?>';
		$.post("php/verify_html.php", {website:website}, function(data){
			if (data == "200"){
				$.post("php/verify_success.php", {id:id}, function(d){
					window.location.href = "my_websites.php";
				});
			}else{
				$('#error').text("Unable to verify domain, please try again");
				$('#error').slideDown();
			}
		});
	});
	
	$('#verify_meta').click(function(e){
		e.preventDefault();
		var website = '<?php echo $url; ?>';
		var id = '<?php echo $website_id; ?>';
		$.post("php/verify_meta.php", {website:website}, function(data){
			if (data == "success"){
				$.post("php/verify_success.php", {id:id}, function(d){
					window.location.href = "my_websites.php";
				});
			}else{
				$('#error').text("Unable to verify domain, please try again");
				$('#error').slideDown();
			}
		});
	});
});
</script>